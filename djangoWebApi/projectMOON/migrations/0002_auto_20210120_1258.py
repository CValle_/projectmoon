# Generated by Django 3.1.4 on 2021-01-20 12:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('projectMOON', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trade',
            name='id_user_owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='projectMOON.user'),
        ),
    ]
