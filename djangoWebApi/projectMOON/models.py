from django.db import models

# Ejecutar cada vez que se modifiquen los modelos
# $ python manage.py makemigrations projectMOON
# $ python manage.py migrate


class User(models.Model):
    # max_length required
    username = models.CharField(max_length=25, unique=True)
    gems = models.IntegerField(default=0)
    last_roll = models.DateTimeField(
        null=True, blank=True
    )  # Actualizar campo manualmente para evitar bugs
    # Currently accepted standards for hashing passwords create a new 16 character long salt
    salt = models.CharField(max_length=16)
    final_pass_sha = models.CharField(max_length=40)

    def __str__(self):
        return self.username


class Session(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    cookie = models.CharField(max_length=500)
    expiration_date = models.DateTimeField()

    def __str__(self):
        return "Session No: " + str(self.pk)


class Family(models.Model):
    name = models.CharField(max_length=25, unique=True)
    long_description = models.CharField(max_length=335)
    url_shield = models.URLField(max_length=200)

    def __str__(self):
        return self.name


class Card(models.Model):
    family = models.ForeignKey(
        Family, related_name='%(class)ss', on_delete=models.CASCADE
    )
    name = models.CharField(max_length=35, unique=True)
    description = models.CharField(max_length=150)
    gems = models.IntegerField(default=0)
    url_image = models.URLField(max_length=200)
    creation_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name + " (" + str(self.id) + ")"


class User_card(models.Model):
    user = models.ForeignKey(User, related_name='%(class)ss', on_delete=models.CASCADE)
    card = models.ForeignKey(Card, related_name='%(class)ss', on_delete=models.CASCADE)

    def __str__(self):
        return (
            str(self.user.username)
            + " owns "
            + str(self.card.name)
            + " ("
            + str(self.card.id)
            + ")"
        )


class Trade(models.Model):
    user_owner = models.ForeignKey(User, on_delete=models.CASCADE)
    # https://docs.djangoproject.com/en/dev/topics/db/models/#be-careful-with-related-name-and-related-query-name
    card_ask = models.ForeignKey(
        Card, related_name='%(class)s_asked', on_delete=models.CASCADE
    )
    card_offer = models.ForeignKey(
        Card, related_name='%(class)s_offered', on_delete=models.CASCADE
    )
    gem_ask = models.IntegerField(default=0)
    gem_offer = models.IntegerField(default=0)
    expiration_date = models.DateTimeField()

    def __str__(self):
        return "Trade No: " + str(self.pk)
