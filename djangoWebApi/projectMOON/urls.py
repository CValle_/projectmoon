from django.urls import path
from . import views
from . import rest_facade

urlpatterns = [
    path('user/<username>/session', rest_facade.session, name='session'),
    path('user/<username>', rest_facade.user, name='user'),
    path('user/<username>/inventory', rest_facade.inventory, name='inventory'),
    path('user/<username>/roll', rest_facade.roll, name='roll'),
    path('trade_list', rest_facade.trade_list, name='trade_list'),
    path(
        'user/<username>/trade/<trade_id>',
        rest_facade.accept_trade,
        name='accept_trade',
    ),
    path('user/<username>/trade', rest_facade.create_trade, name='create_trade'),
    path('user/<username>/card/<id_card>', rest_facade.card, name='card'),
    path('', views.index, name='index'),
    path('families/<family_id>', views.families, name='families'),
    path('users/', views.users, name='users'),
    path('wiki/', views.wiki, name='wiki'),
    path('under_construction/', views.under_construction, name='under_construction'),
    path('cards', rest_facade.cards, name='cards'),
]
