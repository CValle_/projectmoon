import json
import hashlib
import secrets
import datetime
import random
from django.db.models.query_utils import Q


from django.http import JsonResponse, HttpResponseNotAllowed
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.db import IntegrityError

from .models import Trade, User, Session, Card, User_card


# Constantes para evitar cometer errores y facilitar la lectura
DELETE = 'DELETE'
GET = 'GET'
POST = 'POST'
PUT = 'PUT'

# HTTP Status
OK = 200
CREATED = 201
BAD_REQUEST = 400
NOT_FOUND = 404
UNAUTHORIZED = 401
CONFLICT = 409
FORBIDEN = 403


def _check_cookie(user_obj, cookie_header):
    # filter devuelve una lista
    session_obj_list = Session.objects.filter(user__exact=user_obj)
    session_ok = False
    session_obj = None
    for session in session_obj_list:
        # cogemos la sesión indicada de la bbdd y la comparamos con la del usuario
        if session.cookie == cookie_header:
            session_ok = True
            session_obj = session
            break
    return session_ok, session_obj


@csrf_exempt
# request siempre +  lo que haya en el PATH param
def session(request, username):
    if request.method == POST:
        # en el request body está la contraseña
        return _login(request, username)
    elif request.method == DELETE:
        # en el logout el request header está la cookie
        return _logout(request, username)
    else:
        # lista de los métodos HTTP soportados
        return HttpResponseNotAllowed([POST, DELETE])


def _login(request, username):
    try:
        user_obj = User.objects.get(username__iexact=username)
    except User.DoesNotExist:
        return JsonResponse(
            {"errorDescription": "Usuario no encontrado. Regístrese primero"},
            status=NOT_FOUND,
        )

    try:
        # Obtenemos la contraseña (hasheada en cliente) en json y la guardamos en request_body
        request_body = json.loads(request.body)
        if 'password_sha' not in request_body:
            raise ValueError
    except ValueError:
        return JsonResponse(
            {"errorDescription": "Petición no válida. Inténtelo de nuevo"},
            status=BAD_REQUEST,
        )

    hashed_pass = request_body.get('password_sha')
    salted_hashed_pass = hashed_pass + user_obj.salt
    # https://medium.com/better-programming/how-to-hash-in-python-8bf181806141
    # https://docs.python.org/3/library/hashlib.html
    # a hashlib le tiene que entrar un binario -> str.encode()
    final_hashed_pass = hashlib.sha1(str.encode(salted_hashed_pass)).hexdigest()

    db_final_hashed_pass = user_obj.final_pass_sha
    #Comprobamos que la contraseña que obtenemos al hashear la del usuario concuerde con la de la bbdd
    if db_final_hashed_pass == final_hashed_pass:
        # https://docs.djangoproject.com/en/3.1/topics/i18n/timezones/
        expires = timezone.now() + datetime.timedelta(days=10)
        # https://docs.python.org/3/library/secrets.html
        cookie = str(user_obj.id) + '-' + (secrets.token_urlsafe(64))
        response = {
            "id_user": user_obj.id,
            "cookie": cookie,
            "expiration_date": expires.timestamp(),
        }

        #Se crea una nueva sesion en la tabala
        new_session = Session(user=user_obj, expiration_date=expires, cookie=cookie)
        new_session.save()
        return JsonResponse(response, status=OK)

    return JsonResponse(
        {"errorDescription": "La contraseña no coincide"}, status=UNAUTHORIZED
    )


def _logout(request, username):
    try:
        user_obj = User.objects.get(username__iexact=username)
    except User.DoesNotExist:
        return JsonResponse(
            {"errorDescription": "Usuario no encontrado. Regístrese primero"},
            status=NOT_FOUND,
        )

    # NO ESTÁ LA COOKIE EN REQUEST HEADERS POR ALGÚN MOTIVO
    # print(request.META)
    if 'cookie' not in request.headers:
        return JsonResponse(
            {"errorDescription": "Petición no válida. Inténtelo de nuevo"},
            status=BAD_REQUEST,
        )

    cookie_header = request.headers.get('cookie')

    session_ok, session_obj = _check_cookie(user_obj, cookie_header)

    #Mandamos la cookie para verificar que la sesión sea valida
    if session_ok is False:
        return JsonResponse(
            {"errorDescription": "Sesión caducada"}, status=UNAUTHORIZED
        )

    #Se borra la tupla de esta sesion en concreto
    session_obj.delete()

    return JsonResponse(
        {"errorDescription": "Sesión eliminada correctamente"}, status=OK
    )


# user/<username>
@csrf_exempt
#Usamos un metodo para acceder a los metodos de login y logout porque tienen la misma ruta
def user(request, username):
    if request.method == POST:
        return _register_user(username, request)
    elif request.method == GET:
        return _get_user(username)
    else:
        return HttpResponseNotAllowed([GET, POST])


def _register_user(username, request):
    try:
        # Obtenemos la contraseña (hasheada en cliente) en json y la guardamos en request_body
        request_body = json.loads(request.body)
        if 'password_sha' not in request_body:
            raise ValueError
    except ValueError:
        return JsonResponse(
            {"errorDescription": "Petición no válida. Inténtelo de nuevo"},
            status=BAD_REQUEST,
        )

    # CREAMOS EL USER
    salt = secrets.token_urlsafe(16)[:16]

    hashed_pass = request_body.get('password_sha')
    salted_hashed_pass = hashed_pass + salt
    final_hashed_pass = hashlib.sha1(str.encode(salted_hashed_pass)).hexdigest()

    # https://stackoverflow.com/questions/27729487/how-do-catch-a-unique-constraint-failed-404-in-django
    try:
        new_user = User(
            username=username, gems=0, salt=salt, final_pass_sha=final_hashed_pass
        )
        new_user.save()
    except IntegrityError:
        return JsonResponse(
            {"errorDescription": "Usuario ya registrado."},
            status=CONFLICT,
        )

    # GENERAMOS LA RESPONSE
    expires = timezone.now() + datetime.timedelta(days=10)
    cookie = str(new_user.id) + '-' + (secrets.token_urlsafe(64))

    response = {
        "id_user": new_user.id,
        "cookie": cookie,
        "expiration_date": expires.timestamp(),
    }

    new_session = Session(user=new_user, expiration_date=expires, cookie=cookie)
    new_session.save()

    return JsonResponse(response, status=OK)


def _get_user(username):
    
    #declaramos el usuario cogiendolo del header y comparandolo en la bbdd
    try:
        user = User.objects.get(username__iexact=username)
    except User.DoesNotExist:
        return JsonResponse(
            {"errorDescription": f"Usuario {username} no encontrado"},
            status=NOT_FOUND,
        )
    last_roll_milliseconds = None
    if user.last_roll:
        #Pasamos la ultima vez que el usuario hizo una tirada a milisegundos
        last_roll_milliseconds = _to_milliseconds(user.last_roll)
    user_response = {
        "user_id": user.id,
        "username": username,
        "gems": user.gems,
        "last_roll": last_roll_milliseconds,
    }

    return JsonResponse(user_response, status=OK)


def _to_milliseconds(datetime):
    # https://stackoverflow.com/questions/6999726/how-can-i-convert-a-datetime-object-to-milliseconds-since-epoch-unix-time-in-p#comment43779166_23004143
    #Fixes para que la datetime se pase bien a milisegundos
    return int(datetime.replace(tzinfo=timezone.utc).timestamp() * 1000)

@csrf_exempt
def inventory(request, username):
    if request.method != GET:
        return HttpResponseNotAllowed([GET])

    try:
        user_obj = User.objects.get(username__iexact=username)
    except User.DoesNotExist:
        return JsonResponse(
            {"errorDescription": "Usuario no encontrado. Regístrese primero"},
            status=NOT_FOUND,
        )

    if 'cookie' not in request.headers:
        return JsonResponse(
            {"errorDescription": "Petición no válida. Inténtelo de nuevo"},
            status=BAD_REQUEST,
        )

    cookie_header = request.headers.get('cookie')
    session_ok, _ = _check_cookie(user_obj, cookie_header)

    if session_ok is False:
        return JsonResponse(
            {"errorDescription": "La sesión no es válida. Inténtelo de nuevo"},
            status=UNAUTHORIZED,
        )

    query = request.GET.get('q', None)

    # Si hay query se filtra por nombre de carta, si no se cogen todos los trades
    if query is not None:
        # Se pueden obtener datos basados en un filtro con condición OR
        # https://stackoverflow.com/questions/739776/how-do-i-do-an-or-filter-in-a-django-query?answertab=votes#tab-top
        user_card_obj_list = User_card.objects.filter(user=user_obj, card__name__icontains = query)
    else:
        user_card_obj_list = User_card.objects.filter(user=user_obj)

    cards_jsonarr = []

    # Se transforman los objetos Trade en diccionarios que se meten en una lista
    for user_card_obj in user_card_obj_list:
        card_json = {
            "card_id": user_card_obj.card.id,
            "family_id": user_card_obj.card.family.id,
            "name": user_card_obj.card.name,
            "description": user_card_obj.card.description,
            "gems": user_card_obj.card.gems,
            "url_image": user_card_obj.card.url_image,
            "url_shield": user_card_obj.card.family.url_shield,
        }
        cards_jsonarr.append(card_json)

    return JsonResponse(cards_jsonarr, safe=False, status=OK)

@csrf_exempt
def roll(request, username):
    if request.method != POST:
        return HttpResponseNotAllowed([POST])

    try:
        user_obj = User.objects.get(username__iexact=username)
    except User.DoesNotExist:
        return JsonResponse(
            {"errorDescription": "Usuario no encontrado. Regístrese primero"},
            status=NOT_FOUND,
        )

    if 'cookie' not in request.headers:
        return JsonResponse(
            {"errorDescription": "Petición no válida. Inténtelo de nuevo"},
            status=BAD_REQUEST,
        )

    cookie_header = request.headers.get('cookie')
    # Se pone barra baja en lugar de un nombre de variable si no se va a usar
    # cuando la función llamada devuelve múltiples valores
    # Aquí session_obj no se usa
    session_ok, _ = _check_cookie(user_obj, cookie_header)

    if session_ok is False:
        return JsonResponse(
            {"errorDescription": "La sesión no es válida. Inténtelo de nuevo"},
            status=UNAUTHORIZED,
        )

    # https://docs.python.org/es/3/library/datetime.html#available-types
    if user_obj.last_roll is not None:
        if (timezone.now()) - (user_obj.last_roll) < datetime.timedelta(minutes=60):
            return JsonResponse(
                {"errorDescription": "Espera insuficiente"}, status=UNAUTHORIZED
            )
    else:
        user_obj.last_roll = timezone.now()
        user_obj.save()

    try:
        card_obj_list = Card.objects.all()
    except Card.DoesNotExist:
        return JsonResponse(
            {"errorDescription": "Cartas no encontradas."},
            status=NOT_FOUND,
        )
    # int i=0 :C sad java noices
    response = {"id_user": user_obj.id, "results": []}

    i = 0
    while i < 9:
        card_obj = random.choice(card_obj_list)
        result = {
            "id_card": card_obj.id,
            "name": card_obj.name,
            "description": card_obj.description,
            "url_image": card_obj.url_image,
            "gems": card_obj.gems,
        }
        response["results"].append(result)
        i += 1

    user_obj.last_roll = timezone.now()
    user_obj.save()

    return JsonResponse(response, status=CREATED)


@csrf_exempt
def card(request,username,id_card):
    #Hacemos lo mismo que en session porque tienen la mima ruta
    if request.method == POST:
        return choose_card(request,username,id_card)
    elif request.method == DELETE:
        return refund_card(request,username,id_card)
    else:
        return HttpResponseNotAllowed([POST, DELETE])

def choose_card(request, username, id_card):
    try:
        user_obj = User.objects.get(username__iexact=username)
    except User.DoesNotExist:
        return JsonResponse(
            {"errorDescription": "Usuario no encontrado. Regístrese primero"},
            status=NOT_FOUND,
        )

    try:
        card_obj = Card.objects.get(id=int(id_card))
    except Card.DoesNotExist:
        return JsonResponse(
            {"errorDescription": "Carta no encontrada"},
            status=NOT_FOUND,
        )

    if 'cookie' not in request.headers:
        return JsonResponse(
            {"errorDescription": "Petición no válida. Inténtelo de nuevo"},
            status=BAD_REQUEST,
        )

    cookie_header = request.headers.get('cookie')
    session_ok, _ = _check_cookie(user_obj, cookie_header)

    if session_ok is False:
        return JsonResponse(
            {"errorDescription": "La sesión no es válida. Inténtelo de nuevo"},
            status=UNAUTHORIZED,
        )

    # lineas_list es una lista que representa la tabla user_card de este usuario en concreto.
    lineas_list = User_card.objects.filter(user=user_obj)
    # linea representa cada tupla de la misma tabla.
    for linea in lineas_list:

        if linea.card == card_obj:
            user_obj.gems += linea.card.gems
            user_obj.save()
            return JsonResponse(
                {
                    "errorDescription": "You already have that card. Gems"
                    + " will be added to your wallet"
                },
                status=CONFLICT,
            )

    new_user_card = User_card(user=user_obj, card=card_obj)
    new_user_card.save()

    return JsonResponse(
        {"errorDescription": "Selected card has been added to your inventory"},
        status=OK,
    )


def refund_card(request,username, id_card):
    try:
        user_obj = User.objects.get(username__iexact=username)
    except User.DoesNotExist:
        return JsonResponse(
            {"errorDescription": "Usuario no encontrado. Regístrese primero"},
            status=NOT_FOUND,
        )

    try:
        card_obj = Card.objects.get(id=int(id_card))
    except Card.DoesNotExist:
        return JsonResponse(
            {"errorDescription": "Carta no encontrada"},
            status=NOT_FOUND,
        )

    if 'cookie' not in request.headers:
        return JsonResponse(
            {"errorDescription": "Petición no válida. Inténtelo de nuevo"},
            status=BAD_REQUEST,
        )
    cookie_header = request.headers.get('cookie')
        
    session_ok, _ = _check_cookie(user_obj, cookie_header)

    if session_ok is False:
        return JsonResponse(
            {"errorDescription": "La sesión no es válida. Inténtelo de nuevo"},
            status=UNAUTHORIZED,
        )

    #Hacemos una lista con todas las cartas para saber que cartas puedes vender o no
    lineas_list = User_card.objects.filter(user=user_obj)
    asker_has_card = False

    for linea in lineas_list:

        if linea.card == card_obj:
            asker_has_card = True
    
    if asker_has_card == False:
        return JsonResponse(
            {"errorDescription": "Debes tener la carta que quieres vender"},
            status=NOT_FOUND,
        )
    else:
        user_obj.gems += linea.card.gems
        user_obj.save()
        refund_user_card = User_card.objects.get(card = card_obj, user = user_obj)
        refund_user_card.delete()

    return JsonResponse(
        {"errorDescription": "Selected card was refunded"},
        status=OK,
    )


def trade_list(request):
    if request.method != GET:
        return HttpResponseNotAllowed([GET])

    query = request.GET.get('q', None)

    # Si hay query se filtra por nombre de carta, si no se cogen todos los trades
    if query is not None:
        # Se pueden obtener datos basados en un filtro con condición OR
        # https://stackoverflow.com/questions/739776/how-do-i-do-an-or-filter-in-a-django-query?answertab=votes#tab-top
        trades = Trade.objects.filter(
            Q(card_ask__name__icontains = query) | Q(card_offer__name__icontains = query)
        )
    else:
        trades = Trade.objects.all()
    trades_json = []

    # Se transforman los objetos Trade en diccionarios que se meten en una lista
    for trade in trades:
        trade_json = {
            "user": trade.user_owner.username,
            "trade_id": trade.id,
            "gem_ask": trade.gem_ask,
            "gem_offer": trade.gem_offer,
            "id_card_ask": trade.card_ask.id,
            "url_card_ask": trade.card_ask.url_image,
            "id_card_offer": trade.card_offer.id,
            "url_card_offer": trade.card_offer.url_image,
        }
        trades_json.append(trade_json)

    return JsonResponse(trades_json, safe = False, status = OK)


@csrf_exempt
def accept_trade(request, username, trade_id):
    if request.method != POST:
        return HttpResponseNotAllowed([POST])

    try:
        user_obj = User.objects.get(username__iexact=username)
    except User.DoesNotExist:
        return JsonResponse(
            {"errorDescription": "Usuario no encontrado. Regístrese primero"},
            status=NOT_FOUND,
        )

    if 'cookie' not in request.headers:
        return JsonResponse(
            {"errorDescription": "Peticion no valida. Intentalo de nuevo"},
            status=BAD_REQUEST,
        )

    cookie_header = request.headers.get('cookie')
    #Sacamos el id del usuario por el prefijo de la cookie
    cookie_header_index=cookie_header.find('-')

    #igualamos a -1 porque el .find si no encuentra lo que busca devuelve -1
    if cookie_header_index == -1:
        return JsonResponse(
            {"errorDescription": "Peticion no valida, Intentalo de nuevo"}, 
            status=BAD_REQUEST
        )
    
    #Sacamos el usuario que acepta el trade a traves de la cookie de sesion
    cookie_header_index_number=cookie_header[:cookie_header_index]
    user_ask_id = int(cookie_header_index_number)

    try:
        user_ask = User.objects.get(id = user_ask_id)
    except User. DoesNotExist:
        return JsonResponse(
            {"errorDescription": "Usuario no encontrado. Regístrese primero"},
            status = NOT_FOUND,
        )

    #Comprobamos que el que acepta el trade no sea el que lo propuso
    if user_obj == user_ask:
        return JsonResponse(
            {"errorDescription": "No puedes aceptar tu propio trade."},
            status = CONFLICT,
        )

    session_ok, _ = _check_cookie(user_ask, cookie_header)

    if session_ok is False:
        return JsonResponse(
            {"errorDescription": "Sesion caducada"}, status=UNAUTHORIZED
        )

    # Obtener el trade
    try:
        trade = Trade.objects.get(id=trade_id)
    except Trade.DoesNotExist:
        return JsonResponse(
            {"errorDescription": "No existe el trade"}, status=NOT_FOUND
        )
    
    #Comprobamos que user_ask (Clara) tenga la carta pedida (L) y no la ofrecida (C)
    lineas_list = User_card.objects.filter(user=user_ask)
    asker_has_asked_card = False
    asker_has_offered_card = False

    for linea in lineas_list:
        if linea.card == trade.card_ask:
            asker_has_asked_card = True
        if linea.card == trade.card_offer:
            asker_has_offered_card = True

    if not asker_has_asked_card:
        return JsonResponse(
            {"errorDescription": "No tienes la carta requerida para aceptar"},
            status = CONFLICT
        )

    if asker_has_offered_card:
        return JsonResponse(
            {"errorDescription": "Ya tienes la carta ofrecida"},
            status = CONFLICT
        )
    
    #Hay que comprobar también que no se tiene la carta que te ofrecen
    lineas_list = User_card.objects.filter(user=trade.user_owner)
    offerer_has_offered_card = False
    offerer_has_asked_card = False

    for linea in lineas_list:
        if linea.card == trade.card_offer:
            offerer_has_offered_card = True
        if linea.card == trade.card_ask:
            offerer_has_asked_card = True
    
    if not offerer_has_offered_card:
        return JsonResponse(
            {"errorDescription": "El que propone el trade ya no tiene la carta ofrecida"},
            status = NOT_FOUND
        )

    if offerer_has_asked_card:
        return JsonResponse(
            {"errorDescription": "El que propone el trade ya tiene la carta que pedía"},
            status = NOT_FOUND
        )

    # EN ESTE PUNTO ToDO ESTÁ OK

    # Se coge la user_card del usuario que acepta el trade, es decir, la carta pedida
    # asked_user_card = user_ask.user_cards.get(card=trade.card_ask)
    asked_user_card = User_card.objects.get(user=user_ask, card=trade.card_ask)

    # Se coge la user_card del usuario que puso el trade, es decir, la ofrecida
    # offered_user_card = trade.user_owner.user_cards.get(card=trade.card_offer)
    offered_user_card = User_card.objects.get(user=trade.user_owner, card=trade.card_offer)

    # Se intercambian los dueños de las cartas
    asked_user_card.user = trade.user_owner
    offered_user_card.user = user_ask

    # Se guarda el intercambio
    asked_user_card.save()
    offered_user_card.save()

    #Caso de que el que oferta la carta pida gemas
    if user_ask.gems < trade.gem_ask:
        return JsonResponse(
            {"errorDescription": "No dispones de gemas suficientes"},
            status = FORBIDEN
        )
    else:
        #Se sustraen las gemas del pagador.
        user_ask.gems -= trade.gem_ask
        user_ask.save()
        #Se añaden las gemas al cobrador.
        trade.user_owner.gems += trade.gem_ask
        trade.user_owner.save()

    #ahora vamos con el caso de que el que oferta la carta regale gemas
    user_ask.gems += trade.gem_offer
    user_ask.save()
    trade.user_owner.gems -= trade.gem_offer
    trade.user_owner.save()

    print("Intercambio finalizado con éxito")
    # Se elimina el trade
    trade.delete()

    return JsonResponse({}, status = OK)
        
        

    
@csrf_exempt
def create_trade(request, username):
    if request.method != POST:
        return HttpResponseNotAllowed([POST])
    try:
        user_obj = User.objects.get(username__iexact=username)
    except User.DoesNotExist:
        return JsonResponse(
            {"errorDescription": "Usuario no encontrado. Regístrese primero"},
            status = NOT_FOUND,
        )

    if 'cookie' not in request.headers:
        return JsonResponse(
            {"errorDescription": "Petición no válida. Inténtelo de nuevo"},
            status = BAD_REQUEST,
        )

    cookie_header = request.headers.get('cookie')

    session_ok, _ = _check_cookie(user_obj, cookie_header)

    if session_ok is False:
        return JsonResponse(
            {"errorDescription": "Sesión caducada"}, status=UNAUTHORIZED
        )

    try:
        request_body = json.loads(request.body)
    except ValueError:
        return JsonResponse(
            {"errorDescription": "Petición no válida. Inténtelo de nuevo"},
            status=BAD_REQUEST,
        )
    #Collemos a carta que quere o que ofrece o trade
    trade = Trade()
    try:
        trade.card_ask = Card.objects.get(id = int(request_body["id_card_ask"]))
    except Card.DoesNotExist:
        return JsonResponse(
            {"errorDescription": "No existe la carta que pides"},
            status=NOT_FOUND
        )
    
    #Collemos a carta que ofrece o que ofrece o trade
    try:
        card_offer_obj = Card.objects.get(id = int(request_body["id_card_offer"]))
    except Card.DoesNotExist:
        return JsonResponse(
            {"errorDescription": "No existe la carta que ofreces"},
            status=NOT_FOUND
        )

    #Comprobamos se o que publica o trade ten a carta que ofrece
    lineas_list = User_card.objects.filter(user=user_obj)
    asker_has_card = False

    for linea in lineas_list:
        if linea.card == card_offer_obj:
            asker_has_card = True

    if asker_has_card == True:
            trade.card_offer = card_offer_obj
    else:
        return JsonResponse(
            {"errorDescription": "No tienes la carta que ofreces"},
            status = NOT_FOUND
        )

    trade.gem_ask = request_body["gem_ask"]
    if (user_obj.gems >= request_body["gem_offer"]):
        trade.gem_offer = request_body["gem_offer"]
    else:
        return JsonResponse(
            {"errorDescription": "No dispones de gemas suficientes"},
            status=FORBIDEN
        )
    trade.expiration_date = timezone.now() + datetime.timedelta(days=7)
    trade.user_owner = user_obj
    trade.save()

    return JsonResponse({"id": trade.id}, status=OK)

def cards(request):
    if request.method != GET:
        return HttpResponseNotAllowed([GET])

    query = request.GET.get('q', None)

    if query is not None:

        card_obj_list = Card.objects.filter(name__icontains = query)
    else:
        card_obj_list = Card.objects.all()
        
    cards_json = []

    # Se transforman los objetos Card en diccionarios que se meten en una lista
    for card in card_obj_list:
        card_json = {
            "card_id": card.id,
            "family_id": card.family.id,
            "name": card.name,
            "description": card.description,
            "gems": card.gems,
            "url_image": card.url_image,
            "url_shield": card.family.url_shield,
        }
        cards_json.append(card_json)

    return JsonResponse(cards_json, safe = False, status = OK)
