from django.shortcuts import render
from django.http import HttpResponse

# from django.views import generic
from django.template import loader
from projectMOON.models import Family, User


# Devuelve todas las familias en un diccionario con la clave 'families'
def _get_navbar_context():
    families = Family.objects.order_by('name').all()
    context = {'families': families}
    return context


def index(request):
    template = loader.get_template('index.html')
    return HttpResponse(template.render(_get_navbar_context(), request))


def families(request, family_id):
    template = loader.get_template('families.html')
    context = _get_navbar_context()

    family = Family.objects.get(id=family_id)

    context['current_family'] = family
    return HttpResponse(template.render(context, request))


def users(request):
    template = loader.get_template('users.html')
    context = _get_navbar_context()

    # https://stackoverflow.com/questions/9834038/django-order-by-query-set-ascending-and-descending?answertab=votes#tab-top
    users = User.objects.order_by('-gems').all()

    context['users'] = users
    return HttpResponse(template.render(context, request))


def _get_under_construction_response(request):
    template = loader.get_template('under_construction.html')
    context = _get_navbar_context()

    return HttpResponse(template.render(context, request))


def wiki(request):
    return _get_under_construction_response(request)


def under_construction(request):
    return _get_under_construction_response(request)
