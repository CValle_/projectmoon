from django.contrib import admin

from .models import User, Session, Family, Card, User_card, Trade

admin.site.register(User)
admin.site.register(Session)
admin.site.register(Family)
admin.site.register(Card)
admin.site.register(User_card)
admin.site.register(Trade)