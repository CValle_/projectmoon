import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

//The Stateless widget class don't change, contains other widgets
class MyApp extends StatelessWidget {
  @override
  //Pinta la aplicación
  Widget build(BuildContext context) {
    //Widget principal que contiene toda la aplicación (on create en android)
    return MaterialApp(
      //title: 'Pantalla de inicio',
      //Default visual properties (the theme of the app)
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
      //Widget for the default route of the app (/)
      home: MyHomePage(title: 'ProjectMOON'),
    );
  }
}

//The Stateful widget class might change during the lifetime of the widget
class MyHomePage extends StatefulWidget {
  //CONSTRUCTOR
  MyHomePage(
      {
      //id
      Key key,
      //title
      this.title})
      : super(key: key);

  //ATRIBUTOS
  final String title;

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  @override
  //That class will make changes and return the state updated
  _MyHomePageState createState() => _MyHomePageState();
}

//Those changing classes always extends State<ChangingStatefulWidget>
class _MyHomePageState extends State<MyHomePage> {
  //ATRIBUTOS
  int _counter = 0;

  void _incrementCounter() {
    //To make changes on the state (causes to rerun the method)
    setState(() {
      //If we changed _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  // This method is rerun every time setState is called
  Widget build(BuildContext context) {
    //Scaffold class implements the basic material design visual layout structure.
    return Scaffold(
      //This is the header bar with the title from MyHomePageObject
      appBar: AppBar(
        title: Text(widget.title),
      ),
      // Center is a layout widget. It takes a single child and positions it in the middle of the parent
      body: Center(
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "Toggle Debug Paint (right button in the bar)
          // to see the wireframe for each widget.
          //
          //center the children vertically
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              //The dollar writes what is inside the variable, not the text
              '$_counter',
              //Style of the line               //Thickness
              style: Theme.of(context).textTheme.headline3,
            ),
          ],
        ),
      ),
      //Button
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        //Plus sign
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
