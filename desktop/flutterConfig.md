# Configurar Flutter en MacOs

### Introducción
Este enlace te ayudará a seguir toda la información para empezar a construír tu aplicación de escritorio con flutter

https://flutter.dev/desktop

### Prerrequisitos
TENER INSTALADO EL SDK DE FLUTTER:

https://flutter.dev/docs/get-started/install

* Lo descargamos y ejecutamos los siguientes comandos
```
$ cd /Users/clara/Projects
$ unzip ~/Downloads/flutter_macos_1.22.5-stable.zip

#Export -> la variable está disponible en todas las terminales de la sesión (mientras no se reinicie)
#`pwd` -> ejecutar 'ruta del directorio actual' antes de formar el string
$ export PATH="$PATH:`pwd`/flutter/bin"

$ echo $PATH  //(Para comprobar si se ha añadido)
```
* Ejecute el siguiente comando para ver si hay alguna dependencia que necesite instalar
```
$ flutter doctor
```
* Para guardar permanentemente la carpeta en el PATH ejecutamos:
```
$ nano /etc/paths
```
* Añadir la siguiente linea
```
 [PATH_TO_FLUTTER]/flutter/bin
```
* Para comprobar donde has ubicado flutter
```
$ which flutter
```

PARA DESARROLLO EN IOS DESCARGAR:
* XCODE
  https://developer.apple.com/download/release/

* En caso de no tenerlos instalados, instalar Ruby development headers
```
$ xcode-select --install
$ sudo xcodebuild -license
```
* COCOAPODS (para plugins)
```
$ sudo gem install cocoapods

#Para comprobar si se ha instalado correctamente
$ pod --version
```

### Crear un nuevo proyecto (desktop-macOS)
* Realiza los siguientes comandos para asegurarte de que tienes las últimas versiones
  ```
  $ flutter channel dev
  $ flutter upgrade
  $ flutter config --enable-macos-desktop
  ```
* Para asegurarte de que está todo instalado, lista los dispositivos
  ```
  $ flutter devices

  #el resultado debería ser algo como esto
  1 connected device:
  macOS (desktop) • macos • darwin-x64 • Mac OS X 10.15.5 19F101
  ```
* Ejecuta flutter doctor por última vez para ver que todo está correcto
  ```
  $ flutter doctor

  #el resultado debería ser
  · No issues found!
  ```

* To create the app
```
$ flutter create name
$ cd name
$ flutter run
```