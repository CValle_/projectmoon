package com.x.projectmoon.trades;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.x.projectmoon.dtos.TradeDTO;
import com.x.projectmoon.utils.ImageDownloaderThread;
import com.x.projectmoon.R;

import java.net.URL;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<ViewHolderTrade> {
    private Context context;
    private List<TradeDTO> tradeList;

    private ImageView cardOfferIV;
    private ImageView cardAskIV;
    private TextView gemsOfferTV;
    private TextView gemsAskTV;

    public RecyclerAdapter(List<TradeDTO> dataSet, Context context) {
        this.context = context;
        this.tradeList = dataSet;
    }

    @NonNull
    @Override
    public ViewHolderTrade onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Esto genera un item con el xml (cardView)
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trade_item, parent, false);

        return new ViewHolderTrade(view, context);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderTrade holder, int position) {

        cardOfferIV = holder.getImageViewCardOffer();
        cardAskIV = holder.getImageViewCardAsk();
        gemsOfferTV = holder.getTextViewGemsOffer();
        gemsAskTV = holder.getTextViewGemsAsk();

        addInfoItem(position);

        int idTrade = tradeList.get(position).getTradeId();
        String userOwner = tradeList.get(position).getUserOwner();

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(context, AcceptTradeActivity.class);
                intent.putExtra("idTrade", idTrade);
                intent.putExtra("userOwner", userOwner);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return tradeList.size();
    }

    // -- AÑADIR LOS DATOS AL RECYCLERVIEW ---------------------------------------------------------
    public void addInfoItem(int position){
        URL urlImageAsk = tradeList.get(position).getUrlCardAsk();
        URL urlImageOffer = tradeList.get(position).getUrlCardOffer();
        int gemsAsk = tradeList.get(position).getGemAsk();
        int gemsOffer = tradeList.get(position).getGemOffer();

        gemsAskTV.setText(String.valueOf(gemsAsk));
        gemsOfferTV.setText(String.valueOf(gemsOffer));

        ImageDownloaderThread iDT = new ImageDownloaderThread(urlImageAsk, cardAskIV, context);
        iDT.start();
        ImageDownloaderThread iDT2 = new ImageDownloaderThread(urlImageOffer, cardOfferIV, context);
        iDT2.start();
    }
}


