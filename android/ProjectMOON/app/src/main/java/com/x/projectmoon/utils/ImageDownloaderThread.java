package com.x.projectmoon.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;

import com.x.projectmoon.R;

import java.io.IOException;
import java.net.URL;

public class ImageDownloaderThread extends Thread {
    private URL url;
    private ImageView card;
    private Context context;

    public ImageDownloaderThread(URL url, ImageView card, Context context) {
        this.url = url;
        this.card = card;
        this.context = context;
    }

    @Override
    public void run() {
        super.run();

        try {
            Bitmap imageBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());

            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    card.setImageBitmap(imageBitmap);
                }
            });
        } catch (IOException iOE) {
            iOE.printStackTrace();
            //Se tiene que ejecutar en un UIThread para poner tocar los views del hilo principal
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //Se pone la imagen por defecto en caso de que salte la excepción (connection failed)
                    card.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.kallen));
                }
            });

        }


    }
}
