package com.x.projectmoon.utils;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimerThread extends Thread {

    private boolean gachaTime = false;
    private TextView textViewT;
    private Context context;
    private Long timeRoll;
    private String strDate;

    public TimerThread( TextView textViewT, long timeRoll, Context context) {
        this.textViewT = textViewT;
        this.context = context;
        this.timeRoll = timeRoll;
    }

    @Override
    public void run() {
        try {
            do {
                Thread.sleep(1000);
                if (timeRoll != null) {
                    if (timeRoll > 0) {
                        timeRoll -= 1000;
                        DateFormat formatter = new SimpleDateFormat("00:mm:ss");
                        strDate = formatter.format(timeRoll);
                        Log.d("T", strDate);
                    }else{
                        strDate = "¡GACHA TIME!";
                        gachaTime = true;
                    }
                    //Para ejecutar un textview en un hilo secundario
                    ((Activity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            textViewT.setText(strDate);
                        }
                    });
                }
            }while(true);
        } catch (InterruptedException ignored) {}
    }

    public void setGachaTime(boolean gachaTime) {
        this.gachaTime = gachaTime;
    }

    public boolean isGachaTime() {
        return gachaTime;
    }
}
