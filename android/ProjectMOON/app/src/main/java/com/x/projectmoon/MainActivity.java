package com.x.projectmoon;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.x.projectmoon.client.Client;
import com.x.projectmoon.dtos.UserDTO;
import com.x.projectmoon.utils.SharedPreferencesManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

public class MainActivity extends AppCompatActivity {

    static MainActivity mA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mA = this;

        String cookie = SharedPreferencesManager.getInstance().getCookie(getApplicationContext());
        if (cookie != null){
            Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
            startActivity(intent);

            finish();
        }

        setTheme(R.style.Theme_ProjectMOON);
        setContentView(R.layout.activity_main);

        EditText usernameET = findViewById(R.id.editTextUser);
        EditText passwordET = findViewById(R.id.editTextPass);

        Button acceptButton = findViewById(R.id.acceptButton);
        //Cuando se hace click en el botón de información se lanza la clase ActivityInfo
        acceptButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String username = usernameET.getText().toString();
                String password = passwordET.getText().toString();

                if (!username.equals("") && !password.equals("")) {
                    //HACEMOS EL POST
                    postLoginLogic(username, password);
                } else {
                    Toast.makeText(MainActivity.this, "Introduce usuario y contraseña", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button registerButton = findViewById(R.id.registerButton);
        registerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    public void postLoginLogic(String username, String password){
        Client.getInstance(getApplicationContext()).postLoginRest(username, password,
                response -> {
                    UserDTO user = new UserDTO(response);

                    SharedPreferencesManager.getInstance().saveIdUser(user.getIdUser(), getApplicationContext());
                    SharedPreferencesManager.getInstance().saveUsername(username, getApplicationContext());
                    SharedPreferencesManager.getInstance().saveCookie(user.getCookie(), getApplicationContext());
                    SharedPreferencesManager.getInstance().saveExpDate(user.getExpDate(), getApplicationContext());

                    Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                    startActivity(intent);

                    finish();
                },
                error -> {
                    String responseBodyString = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                    try {
                        JSONObject errorResponseBodyJson = new JSONObject(responseBodyString);
                        Toast.makeText(MainActivity.this, (String) errorResponseBodyJson.get("errorDescription"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException jsonE) {
                        jsonE.printStackTrace();
                    }
                });
    }

    public static MainActivity getInstance(){
        return mA;
    }
}
