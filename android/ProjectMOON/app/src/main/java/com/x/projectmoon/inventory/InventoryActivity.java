package com.x.projectmoon.inventory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.x.projectmoon.R;
import com.x.projectmoon.client.Client;
import com.x.projectmoon.dtos.CardListDTO;
import com.x.projectmoon.utils.SharedPreferencesManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

public class InventoryActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    //private List<URL> imgURList;
    private String username;
    private String cookie;
    private CardListDTO cardListDTO;
    private String myQuery;

    static InventoryActivity iA;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        iA = this;

        username = SharedPreferencesManager.getInstance().getUsername(getApplicationContext());
        cookie = SharedPreferencesManager.getInstance().getCookie(getApplicationContext());
        getInventoryLogic();

        setContentView(R.layout.activity_inventory);

        // -- SEARCH VIEW
        SearchView searchInventory = findViewById(R.id.inventorySV);
        searchInventory.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                myQuery = query;
                getInventoryLogic();

                return false;
            }
        });

        searchInventory.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                myQuery = null;
                getInventoryLogic();
                return false;
            }
        });
    }

    public void getInventoryLogic(){
        Client.getInstance(getApplicationContext()).getInventoryRest(username, cookie, myQuery,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    cardListDTO = new CardListDTO(response);

                    RecyclerAdapterInventory myAdapter = new RecyclerAdapterInventory(cardListDTO.getInventoryDTOList(), InventoryActivity.this);

                    //relaciona RecyclerInventory con recicler view
                    recyclerView = findViewById(R.id.inventoryRecyclerView);
                    recyclerView.setAdapter(myAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(InventoryActivity.this));

                }
            },
            new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError error) {
                    String responseBodyString = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                    try {
                        JSONObject errorResponseBodyJson = new JSONObject(responseBodyString);
                        Toast.makeText(InventoryActivity.this, (String) errorResponseBodyJson.get("errorDescription"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException jsonE) {
                        jsonE.printStackTrace();
                    }
                    if (error.networkResponse.statusCode == 401){
                        SharedPreferencesManager.getInstance().deleteFullSession(getApplicationContext());
                    }
                }
            });
    }

    public static InventoryActivity getInstance(){
        return iA;
    }
}