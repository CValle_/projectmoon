package com.x.projectmoon.trades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.x.projectmoon.R;
import com.x.projectmoon.client.Client;
import com.x.projectmoon.utils.SharedPreferencesManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

public class AcceptTradeActivity extends AppCompatActivity {

    private String username;
    private String cookie;
    private int idTrade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        idTrade = getIntent().getIntExtra("idTrade", 0);
        username = getIntent().getStringExtra("userOwner");

        //username = SharedPreferencesManager.getInstance().getUsername(getApplicationContext());
        cookie = SharedPreferencesManager.getInstance().getCookie(getApplicationContext());

        setContentView(R.layout.activity_accept_trade);

        Button chooseButton = findViewById(R.id.acceptTradeButton);

        chooseButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                postAcceptTradeLogic();
                finish();
            }
        });
    }

    public void postAcceptTradeLogic() {
        Client.getInstance(getApplicationContext()).postAcceptTradeRest(username, idTrade, cookie,
            new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Toast.makeText(AcceptTradeActivity.this, "Intercambio realizado correctamente", Toast.LENGTH_SHORT).show();
                    TradesActivity.getInstance().finish();
                }
            },
            new Response.ErrorListener(){

                @Override
                public void onErrorResponse(VolleyError error) {
                    String responseBodyString = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                    try {
                        JSONObject errorResponseBodyJson = new JSONObject(responseBodyString);
                        Toast.makeText(AcceptTradeActivity.this, (String) errorResponseBodyJson.get("errorDescription"), Toast.LENGTH_SHORT).show();

                        if (error.networkResponse.statusCode == 401){
                            //Toast.makeText(AcceptTradeActivity.this, "Sesión inválida", Toast.LENGTH_SHORT).show();
                            SharedPreferencesManager.getInstance().deleteFullSession(getApplicationContext());
                        }
                    } catch (JSONException jsonE) {
                        jsonE.printStackTrace();
                    }
                }
            });
    }
}