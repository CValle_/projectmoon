package com.x.projectmoon.dtos;

import java.net.URL;

public class TradeDTO {

    private String userOwner;
    private int tradeId;
    private int gemAsk;
    private int gemOffer;
    private int idCardAsk;
    private URL urlCardAsk;
    private int idCardOffer;
    private URL urlCardOffer;

    public TradeDTO(String userOwner, int tradeId, int gemAsk, int gemOffer, int idCardAsk, URL urlCardAsk, int idCardOffer, URL urlCardOffer){
        this.userOwner = userOwner;
        this.tradeId = tradeId;
        this.gemAsk = gemAsk;
        this.gemOffer = gemOffer;
        this.idCardAsk = idCardAsk;
        this.urlCardAsk = urlCardAsk;
        this.idCardOffer = idCardOffer;
        this.urlCardOffer = urlCardOffer;
    }

    public String getUserOwner() {
        return userOwner;
    }

    public int getTradeId() {
        return tradeId;
    }

    public int getGemAsk() {
        return gemAsk;
    }

    public int getGemOffer() {
        return gemOffer;
    }

    public int getIdCardAsk() {
        return idCardAsk;
    }

    public URL getUrlCardAsk() {
        return urlCardAsk;
    }

    public int getIdCardOffer() {
        return idCardOffer;
    }

    public URL getUrlCardOffer() {
        return urlCardOffer;
    }
}
