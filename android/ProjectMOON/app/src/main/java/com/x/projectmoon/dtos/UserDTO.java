package com.x.projectmoon.dtos;

import org.json.JSONException;
import org.json.JSONObject;

public class UserDTO {

    private JSONObject response;

    private int idUser;
    private String cookie;
    private String expDate;

    public UserDTO(JSONObject response){
        this.response = response;

        try{
            idUser = response.getInt("id_user");
            cookie = response.getString("cookie");
            expDate = response.getString("expiration_date");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getIdUser() {
        return idUser;
    }

    public String getCookie() {
        return cookie;
    }

    public String getExpDate() {
        return expDate;
    }
}
