package com.x.projectmoon.trades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.x.projectmoon.R;
import com.x.projectmoon.client.Client;
import com.x.projectmoon.dtos.CardDTO;
import com.x.projectmoon.dtos.CardListDTO;
import com.x.projectmoon.utils.SharedPreferencesManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class CreateTradeActivity extends AppCompatActivity {

    private CardListDTO cardListDTO;
    private List<CardDTO> inventoryList;
    private List<CardDTO> allCardsList;
    private List<String> cardNameList;

    private String username;
    private String cookie;

    private AutoCompleteTextView cardAskACTV;
    private AutoCompleteTextView cardOfferACTV;

    private String cardAsk = null;
    private String cardOffer = null;

    private int idCardAsk;
    private int idCardOffer;
    private int gemsAsk;
    private int gemsOffer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_trade);

        username = SharedPreferencesManager.getInstance().getUsername(getApplicationContext());
        cookie = SharedPreferencesManager.getInstance().getCookie(getApplicationContext());

        //EDITTEXT
        EditText gemsAskET = findViewById(R.id.numeroGemasP);
        EditText gemsOfferET = findViewById(R.id.numeroGemasO);
        //AUTOCOMPLETREXTVIEW
        cardAskACTV = findViewById(R.id.nombreCartaP);
        cardOfferACTV = findViewById(R.id.nombreCartaO);

        getInventoryLogic();
        getCardsLogic();

        Button createTradeB = findViewById(R.id.createTradeButton);
        createTradeB.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                cardOffer = cardOfferACTV.getText().toString();
                cardAsk = cardAskACTV.getText().toString();

                idCardAsk = setId(allCardsList, cardAsk);
                idCardOffer = setId(inventoryList, cardOffer);

                //Si las gemas son nulas se pone por defecto 0
                gemsAsk = gemsAskET.getText().toString().equals("") ? 0: Integer.parseInt(gemsAskET.getText().toString());
                gemsOffer = gemsOfferET.getText().toString().equals("") ? 0: Integer.parseInt(gemsOfferET.getText().toString());

                if (!cardOfferACTV.getText().toString().equals("") && !cardAskACTV.getText().toString().equals("")) {
                    postCreateTradeLogic(cookie, idCardAsk, idCardOffer, gemsAsk, gemsOffer);
                    finish();
                } else {
                    Toast.makeText(CreateTradeActivity.this, "Introduce todos los datos requeridos", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    // -- PETICIONES DE OBTENER LISTA -----------------------------------------------------------------------
    public void getInventoryLogic() {

        Client.getInstance(getApplicationContext()).getInventoryRest(username, cookie, null,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    cardListDTO = new CardListDTO(response);
                    inventoryList = cardListDTO.getInventoryDTOList();

                    cardNameList = new ArrayList<>();

                    for (int i = 0; i < inventoryList.size(); i++){
                        //idCardOffer = inventoryList.get(i).getIdCard();
                        cardOffer = inventoryList.get(i).getName();

                        cardNameList.add(cardOffer);
                    }

                    generateDropDown(cardNameList, cardOfferACTV);

                }
            },
            new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError error) {
                    String responseBodyString = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                    try {
                        JSONObject errorResponseBodyJson = new JSONObject(responseBodyString);
                        Toast.makeText(CreateTradeActivity.this, (String) errorResponseBodyJson.get("errorDescription"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException jsonE) {
                        jsonE.printStackTrace();
                    }
                    if (error.networkResponse.statusCode == 401){
                        SharedPreferencesManager.getInstance().deleteFullSession(getApplicationContext());
                    }
                }
            });
    }

    public void getCardsLogic() {

        Client.getInstance(getApplicationContext()).getCardsRest(null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        cardListDTO = new CardListDTO(response);
                        allCardsList = cardListDTO.getInventoryDTOList();

                        cardNameList = new ArrayList<>();

                        for (int i = 0; i < allCardsList.size(); i++){
                            //idCardAsk = allCardsList.get(i).getIdCard();
                            cardAsk = allCardsList.get(i).getName();

                            cardNameList.add(cardAsk);
                        }
                        generateDropDown(cardNameList, cardAskACTV);
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String responseBodyString = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                        try {
                            JSONObject errorResponseBodyJson = new JSONObject(responseBodyString);
                            Toast.makeText(CreateTradeActivity.this, (String) errorResponseBodyJson.get("errorDescription"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException jsonE) {
                            jsonE.printStackTrace();
                        }
                    }
                });
    }

    // -- LOGICA DE CREAR EL TRADE -----------------------------------------------------------------
    public void postCreateTradeLogic(String cookie, int cardAsk, int cardOffer, int gemsAsk, int gemsOffer){
        Client.getInstance(getApplicationContext()).postCreateTradeRest(username, cookie, cardAsk, cardOffer, gemsAsk, gemsOffer,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Toast.makeText(CreateTradeActivity.this, "¡Trade creado!", Toast.LENGTH_SHORT).show();
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    String responseBodyString = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                    try {
                        JSONObject errorResponseBodyJson = new JSONObject(responseBodyString);
                        Toast.makeText(CreateTradeActivity.this, (String) errorResponseBodyJson.get("errorDescription"), Toast.LENGTH_SHORT).show();

                        if (error.networkResponse.statusCode == 401){
                            SharedPreferencesManager.getInstance().deleteFullSession(getApplicationContext());
                        }
                    } catch (JSONException jsonE) {
                        jsonE.printStackTrace();
                    }
                }
            });
    }

    // -- UTILIDADES -------------------------------------------------------------------------------
    public void generateDropDown(List<String> cardNameList, AutoCompleteTextView actv){
        String[] cardNameArr = new String[cardNameList.size()];
        cardNameList.toArray(cardNameArr);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (CreateTradeActivity.this, android.R.layout.select_dialog_item, cardNameArr);

        actv.setThreshold(1);
        actv.setAdapter(adapter);
    }

    public int setId(List<CardDTO> cardList, String card){
        //Nunca va a existir un id -1, por lo que si no pone id, saltaría error 404
        int id = -1;

        for (int i = 0; i < cardList.size(); i++){
            if (cardList.get(i).getName().equals(card))
                id = cardList.get(i).getIdCard();
        }

        return id;
    }
}