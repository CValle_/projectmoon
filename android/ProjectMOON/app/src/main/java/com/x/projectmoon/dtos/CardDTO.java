package com.x.projectmoon.dtos;

import java.net.URL;

public class CardDTO {

    private int idCard;
    private int idFamily;
    private String name;
    private String description;
    private int gems;
    private URL urlImage;
    private URL urlShield;

    public CardDTO(int idCard, int idFamily, String name, String description, int gems, URL urlImage, URL urlShield) {
        this.idCard = idCard;
        this.idFamily = idFamily;
        this.name = name;
        this.description = description;
        this.gems = gems;
        this.urlImage = urlImage;
        this.urlShield = urlShield;
    }

    public int getIdCard() {
        return idCard;
    }

    public int getIdFamily() {
        return idFamily;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getGems() {
        return gems;
    }

    public URL getUrlImage() {
        return urlImage;
    }

    public URL getUrlShield() {
        return urlShield;
    }
}
