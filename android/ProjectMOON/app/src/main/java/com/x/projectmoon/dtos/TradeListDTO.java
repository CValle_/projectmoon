package com.x.projectmoon.dtos;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class TradeListDTO implements Serializable {
    private JSONArray response;

    private List<TradeDTO> tradeDTOList;

    private String userOwner;
    private int tradeId;
    private int gemAsk;
    private int gemOffer;
    private int idCardAsk;
    private URL urlCardAsk;
    private int idCardOffer;
    private URL urlCardOffer;

    public TradeListDTO(JSONArray response){
        this.response = response;

        tradeDTOList = new ArrayList<>();
        try {
            for (int i = 0; i < response.length(); i++) {
                userOwner = response.getJSONObject(i).getString("user");
                tradeId = response.getJSONObject(i).getInt("trade_id");
                gemAsk = response.getJSONObject(i).getInt("gem_ask");
                gemOffer = response.getJSONObject(i).getInt("gem_offer");
                idCardAsk = response.getJSONObject(i).getInt("id_card_ask");
                urlCardAsk = new URL(response.getJSONObject(i).getString("url_card_ask"));
                idCardOffer = response.getJSONObject(i).getInt("id_card_offer");
                urlCardOffer = new URL(response.getJSONObject(i).getString("url_card_offer"));

                tradeDTOList.add(new TradeDTO(userOwner, tradeId, gemAsk, gemOffer, idCardAsk, urlCardAsk, idCardOffer, urlCardOffer));
            }
        } catch (JSONException | MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public List<TradeDTO> getTradeDTOList() {
        return tradeDTOList;
    }
}
