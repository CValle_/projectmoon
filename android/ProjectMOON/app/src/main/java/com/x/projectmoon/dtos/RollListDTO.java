package com.x.projectmoon.dtos;

import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class RollListDTO implements Serializable {

    private JSONObject response;

    private List<RollCardDTO> cardDTOList;

    private int idCard;
    private String name;
    private String description;
    private URL urlImage;
    private int gems;

    public RollListDTO(JSONObject response) {
        this.response = response;

        cardDTOList = new ArrayList<>();
        try {
            JSONArray cardsJsonArr = response.getJSONArray("results");
            for (int i = 0; i < cardsJsonArr.length(); i++) {
                idCard = cardsJsonArr.getJSONObject(i).getInt("id_card");
                name = cardsJsonArr.getJSONObject(i).getString("name");
                description = cardsJsonArr.getJSONObject(i).getString("description");
                urlImage = new URL(cardsJsonArr.getJSONObject(i).getString("url_image"));
                gems = cardsJsonArr.getJSONObject(i).getInt("gems");

                cardDTOList.add(new RollCardDTO(idCard, name, description, urlImage, gems));
            }
        } catch (MalformedURLException | JSONException e) {
            e.printStackTrace();
        }
    }

    public List<RollCardDTO> getCardDTOList() {
        return cardDTOList;
    }
}
