package com.x.projectmoon.client;

import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class JARSubclassCustomHeader extends JsonArrayRequest {
    private String cookie;

    public JARSubclassCustomHeader (int method, String url, String tokenSesion, @Nullable JSONArray jsonRequest, Response.Listener<JSONArray> listener, @Nullable Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
        this.cookie = tokenSesion;
    }
    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> overriddenHeaders = new HashMap<>();
        for (Map.Entry<String, String> entry : super.getHeaders().entrySet()) {
            overriddenHeaders.put(entry.getKey(), entry.getValue());
        }
        if (cookie != null) {
            overriddenHeaders.put("cookie", cookie);
        }
        return overriddenHeaders;
    }
}
