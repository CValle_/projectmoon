package com.x.projectmoon.inventory;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.x.projectmoon.dtos.CardDTO;
import com.x.projectmoon.utils.ImageDownloaderThread;
import com.x.projectmoon.R;

import java.net.URL;
import java.util.List;

public class RecyclerAdapterInventory extends RecyclerView.Adapter<ViewHolderInventory> {
    private Context context;
    private List<CardDTO> inventoryList;

    private ImageView cardIV;
    private TextView gemsTV;
    private TextView nameTV;
    private TextView descriptionTV;
    private Button refundB;

    public RecyclerAdapterInventory (List<CardDTO> dataSet, Context context) {
        this.inventoryList = dataSet;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolderInventory onCreateViewHolder (@NonNull ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_item, parent, false);

        //System.out.println(imageData);
        return new ViewHolderInventory(view);
    }

    @Override
    public void onBindViewHolder (@NonNull ViewHolderInventory holder, int position){

        cardIV = holder.getImageViewCard();
        gemsTV = holder.getTextViewGems();
        nameTV = holder.getTextViewName();
        descriptionTV = holder.getTextViewDescription();
        refundB = holder.getButtonRefund();

        addInfoItem(position);

        int idCard = inventoryList.get(position).getIdCard();

        refundB.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(context, AcceptRefundActivity.class);
                intent.putExtra("idCard", idCard);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount () {
        return inventoryList.size();
    }

    public void addInfoItem(int position){
        URL urlImage = inventoryList.get(position).getUrlImage();
        String name = inventoryList.get(position).getName();
        String description = inventoryList.get(position).getDescription();
        int gems = inventoryList.get(position).getGems();

        gemsTV.setText(String.valueOf(gems));
        nameTV.setText(name);
        descriptionTV.setText(description);

        ImageDownloaderThread iDT3 = new ImageDownloaderThread(urlImage, cardIV, context);
        iDT3.start();
    }
}
