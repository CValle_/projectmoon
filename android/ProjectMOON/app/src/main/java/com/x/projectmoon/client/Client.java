package com.x.projectmoon.client;

import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Client {
	private static Client client;

	//se hace una variable ya que hay que cambiarla según la red donde se ejecute
	//python manage.py runserver <ip:port>
	public static final String ip = "192.168.111.192:8000";

	private RequestQueue queue;

	private Client(Context context) {

		this.queue = Volley.newRequestQueue(context);
	}

	public static Client getInstance(Context context) {
		if (client == null) {
			client = new Client(context);
		}
		return client;
	}

	private static String calculateSHA1(String password) throws NoSuchAlgorithmException {
		MessageDigest crypt = MessageDigest.getInstance("SHA-1");
		crypt.reset();
		crypt.update(password.getBytes(StandardCharsets.UTF_8));

		return new BigInteger(1, crypt.digest()).toString(16);
	}

	// https://developer.android.com/training/volley/request

	//--- LOGIN ------------------------------------------------------------------------------------
	public void postLoginRest(String username, String password, Response.Listener<JSONObject> responseOK, Response.ErrorListener responseError) {
		String url = "http://" + ip + "/user/" + username + "/session";
		JSONObject requestBodyJson = new JSONObject();

		// Enviamos al api la shaPass en el requestBody
		try {
			String shaPass = calculateSHA1(password);
			requestBodyJson.put("password_sha", shaPass);
		} catch (JSONException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		// Pasamos el método, la url definida anteriormente y el JSON
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, requestBodyJson,
				responseOK,
				responseError);
		queue.add(jsonObjectRequest);
	}

	//--- REGISTER ---------------------------------------------------------------------------------
	public void postRegisterRest(String username, String password, Response.Listener<JSONObject> responseOK, Response.ErrorListener responseError) {
		String url = "http://" + ip + "/user/" + username;
		JSONObject requestBodyJson = new JSONObject();

		// Enviamos al api la shaPass en el requestBody
		try {
			String shaPass = calculateSHA1(password);
			requestBodyJson.put("password_sha", shaPass);
		} catch (JSONException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, requestBodyJson,
				responseOK,
				responseError);
		queue.add(jsonObjectRequest);
	}

	//--- MENU -------------------------------------------------------------------------------------
	public void getUserRest(String username, Response.Listener<JSONObject> responseOK, Response.ErrorListener responseError) {
		String url = "http://" + ip + "/user/" + username;
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
				responseOK,
				responseError);
		queue.add(jsonObjectRequest);
	}

	//--- ROLL -------------------------------------------------------------------------------------
	public void postRollRest(String username, String cookie, Response.Listener<JSONObject> responseOK, Response.ErrorListener responseError) {

		String url = "http://" + ip + "/user/" + username + "/roll";

		JORSubclassCustomHeader jsonObjectRequest = new JORSubclassCustomHeader(Request.Method.POST, url, cookie, null,
				responseOK,
				responseError);
		queue.add(jsonObjectRequest);
	}

	//--- ROLL CHOOSE ------------------------------------------------------------------------------
	public void postRollChooseRest(String username, int idCard, String cookie, Response.Listener<JSONObject> responseOK, Response.ErrorListener responseError) {
		String url = "http://" + ip + "/user/" + username + "/card/" + idCard;

		JORSubclassCustomHeader jsonObjectRequest = new JORSubclassCustomHeader(Request.Method.POST, url, cookie, null,
				responseOK,
				responseError);
		queue.add(jsonObjectRequest);
	}

	//--- CARD REFUND ------------------------------------------------------------------------------
	public void deleteCardRest(String username, int idCard, String cookie, Response.Listener<JSONObject> responseOK, Response.ErrorListener responseError) {
		String url = "http://" + ip + "/user/" + username + "/card/" + idCard;

		JORSubclassCustomHeader jsonObjectRequest = new JORSubclassCustomHeader(Request.Method.DELETE, url, cookie, null,
				responseOK,
				responseError);
		queue.add(jsonObjectRequest);
	}

	//--- TRADE LIST ------------------------------------------------------------------------------
	public void getTradeListRest(@Nullable String query, Response.Listener<JSONArray> responseOK, Response.ErrorListener responseError) {

		String url = "http://" + ip + "/trade_list" + ((query != null) ? "?q=" + query : "");
		JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
				responseOK,
				responseError);
		queue.add(jsonArrayRequest);
	}

	//--- ACCEPT TRADE -----------------------------------------------------------------------------
	public void postAcceptTradeRest(String username, int tradeId, String cookie, Response.Listener<JSONObject> responseOK, Response.ErrorListener responseError) {
		String url = "http://" + ip + "/user/" + username + "/trade/" + tradeId;

		JORSubclassCustomHeader jsonObjectRequest = new JORSubclassCustomHeader(Request.Method.POST, url, cookie, null,
				responseOK,
				responseError);
		queue.add(jsonObjectRequest);
	}

	//--- CREATE TRADE -----------------------------------------------------------------------------
	public void postCreateTradeRest(String username, String cookie, int cardAsk, int cardOffer, int gemsAsk, int gemsOffer,
									Response.Listener<JSONObject> responseOK, Response.ErrorListener responseError) {
		String url = "http://" + ip + "/user/" + username + "/trade";
		JSONObject requestBodyJson = new JSONObject();

		try {
			requestBodyJson.put("id_card_ask", cardAsk);
			requestBodyJson.put("id_card_offer", cardOffer);
			requestBodyJson.put("gem_ask", gemsAsk);
			requestBodyJson.put("gem_offer", gemsOffer);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JORSubclassCustomHeader jsonObjectRequest = new JORSubclassCustomHeader(Request.Method.POST, url, cookie, requestBodyJson,
				responseOK,
				responseError);
		queue.add(jsonObjectRequest);

	}

	//--- INVENTORY --------------------------------------------------------------------------------
	public void getInventoryRest(String username, String cookie, @Nullable String query, Response.Listener<JSONArray> responseOK, Response.ErrorListener responseError) {
		String url = "http://" + ip + "/user/" + username + "/inventory" + ((query != null) ? "?q=" + query : "");
		JSONArray requestBodyJson = new JSONArray();

		JARSubclassCustomHeader jsonArrayRequest = new JARSubclassCustomHeader(Request.Method.GET, url, cookie, requestBodyJson,
				responseOK,
				responseError);

		queue.add(jsonArrayRequest);
	}

	//--- ALL CARDS --------------------------------------------------------------------------------
	public void getCardsRest(@Nullable String query, Response.Listener<JSONArray> responseOK, Response.ErrorListener responseError) {
		String url = "http://" + ip + "/cards" + ((query != null) ? "?q=" + query : "");

		JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
				responseOK,
				responseError);
		queue.add(jsonArrayRequest);
	}
}
