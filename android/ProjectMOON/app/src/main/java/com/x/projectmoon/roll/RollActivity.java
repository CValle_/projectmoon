package com.x.projectmoon.roll;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.x.projectmoon.R;
import com.x.projectmoon.client.Client;
import com.x.projectmoon.dtos.RollListDTO;
import com.x.projectmoon.utils.ImageDownloaderThread;
import com.x.projectmoon.utils.SharedPreferencesManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

public class RollActivity extends AppCompatActivity {

    private int layoutWidth;
    private int layoutHeight;
    private String username;
    private String cookie;

    private RollListDTO rollListDTO;

    static RollActivity rA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Cojo el contexto para poder cerrar esta actividad desde otra clase
        rA = this;

        username = SharedPreferencesManager.getInstance().getUsername(getApplicationContext());
        cookie = SharedPreferencesManager.getInstance().getCookie(getApplicationContext());

        //Disposición de las cartas en el layout
        this.layoutWidth = 3;
        this.layoutHeight = 3;

        //Inicio la actividad
        setContentView(R.layout.activity_roll);

        postRollLogic(username);

    }

    public void postRollLogic(String username){
        Client.getInstance(getApplicationContext()).postRollRest(username, cookie,
                response -> {

                    rollListDTO = new RollListDTO(response);
                    createMesh();
                },
                error -> {
                    String responseBodyString = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                    try {
                        //SE OBTIENE EL JSON DEL ERROR
                        JSONObject errorResponseBodyJson = new JSONObject(responseBodyString);
                        Toast.makeText(RollActivity.this, (String) errorResponseBodyJson.get("errorDescription"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException jsonE) {
                        jsonE.printStackTrace();
                    }
                    if (error.networkResponse.statusCode == 401){
                        Toast.makeText(RollActivity.this, "Sesión inválida", Toast.LENGTH_SHORT).show();
                        SharedPreferencesManager.getInstance().deleteFullSession(getApplicationContext());
                    }
                });
    }

    public void createMesh() {
        LinearLayout contenedor = findViewById(R.id.contenedor);

        //Ancho de la pantalla
        int cardSize = Resources.getSystem().getDisplayMetrics().widthPixels / layoutWidth;

        int cardNum = 0;

        for (int alto = 0; alto < layoutHeight; ++alto) {
            //Creando layout horizontal (fila)
            LinearLayout fila = new LinearLayout(this);
            LinearLayout.LayoutParams paramsFila = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            paramsFila.setMargins(0, 0, 0, 50);
            fila.setLayoutParams(paramsFila);
            fila.setOrientation(LinearLayout.HORIZONTAL);

            for (int ancho = 0; ancho < layoutWidth; ++ancho) {
                ImageView card = cardConfig(cardSize, cardNum);
                fila.addView(card);
                cardNum++;
            }
            contenedor.addView(fila);
        }
    }

    public ImageView cardConfig(int tileSize, int cardNum) {

        //Creamos el imageview que contendrá las imágenes
        ImageView card = new ImageView(this);
        LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(tileSize, tileSize);
        card.setLayoutParams(imageParams);

        //Se manda la url, del objeto de la lista de objetos según la posición)
        ImageDownloaderThread iDT = new ImageDownloaderThread(rollListDTO.getCardDTOList().get(cardNum).getUrlImage(), card, this);
        iDT.start();

        //CLICKABLE CARD
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), RollChooseActivity.class);

                intent.putExtra("RollCardDTO", rollListDTO.getCardDTOList().get(cardNum));
                startActivity(intent);
            }
        });

        return card;
    }

    //Método que pasa la instancia de esta clase (para cerrarla desde ChooseCardActivity en este caso)
    public static RollActivity getInstance(){
        return rA;
    }
}
