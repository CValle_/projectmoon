package com.x.projectmoon;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.BoringLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.x.projectmoon.client.Client;
import com.x.projectmoon.dtos.UserDTO;
import com.x.projectmoon.utils.SharedPreferencesManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class RegisterActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_register);

        EditText usernameRegET = findViewById(R.id.editTextUserReg);
        EditText passRegET = findViewById(R.id.editTextPassReg);
        EditText passConfirmET = findViewById(R.id.editTextConfirmPass);

        Button acceptRButton = findViewById(R.id.acceptRButton);

        acceptRButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String username = usernameRegET.getText().toString();
                String password = passRegET.getText().toString();
                String confirmPass = passConfirmET .getText().toString();

                if (!password.equals(confirmPass)) {
                    Toast.makeText(RegisterActivity.this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
                } else if (!username.equals("") && !password.equals(""))  {
                    postRegisterLogic(username, password);
                } else {
                    Toast.makeText(RegisterActivity.this, "Introduce usuario y contraseña", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public void postRegisterLogic (String username, String password){
        Client.getInstance(getApplicationContext()).postRegisterRest(username, password,
                response -> {
                    UserDTO user = new UserDTO(response);

                    SharedPreferencesManager.getInstance().saveIdUser(user.getIdUser(), getApplicationContext());
                    SharedPreferencesManager.getInstance().saveUsername(username, getApplicationContext());
                    SharedPreferencesManager.getInstance().saveCookie(user.getCookie(), getApplicationContext());
                    SharedPreferencesManager.getInstance().saveExpDate(user.getExpDate(), getApplicationContext());

                    boolean firstRoll = true;
                    Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                    intent.putExtra("firstRoll",firstRoll);
                    startActivity(intent);

                    finish();
                    MainActivity.getInstance().finish();
                },
                error -> {
                    String responseBodyString = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                    try {
                        JSONObject errorResponseBodyJson = new JSONObject(responseBodyString);
                        Toast.makeText(RegisterActivity.this, (String) errorResponseBodyJson.get("errorDescription"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException jsonE) {
                        jsonE.printStackTrace();
                    }
                });
    }
}

