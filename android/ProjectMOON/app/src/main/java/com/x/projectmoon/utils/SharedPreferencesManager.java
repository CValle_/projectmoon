package com.x.projectmoon.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesManager {
    private static SharedPreferencesManager singleton = null;

    private static String prefsName = "SAVED_PREFS";
    private static String idUserKey = "ID_USER";
    private static String usernameKey = "USERNAME";
    private static String cookieKey = "COOKIE";
    private static String expDateKey = "EXP_DATE";

    private int idUser;
    private String username;
    private String cookie;
    private String expDate;

    private SharedPreferencesManager() {

    }

    public static SharedPreferencesManager getInstance(){
        if (singleton == null) {
            singleton = new SharedPreferencesManager();
        }
        return singleton;
    }

    // --- ID_USER ---------------------------------------------------------------------------------
    public static void saveIdUser(int idUser, Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE).edit();
        editor.putInt(idUserKey, idUser);
        editor.apply();
    }

    public static int getIdUser(Context context){
        SharedPreferences preferences = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE);
        int idUser = preferences.getInt(idUserKey, 0);
        return idUser;
    }

    public static void deleteIdUser(Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE).edit();
        editor.remove(idUserKey);
        editor.apply();
    }

    // --- USERNAME --------------------------------------------------------------------------------
    public static void saveUsername(String username, Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE).edit();
        editor.putString(usernameKey, username);
        editor.apply();
    }

    public static String getUsername(Context context){
        SharedPreferences preferences = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE);
        String username = preferences.getString(usernameKey, null);
        return username;
    }

    public static void deleteUsername(Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE).edit();
        editor.remove(usernameKey);
        editor.apply();
    }

    // --- COOKIE ----------------------------------------------------------------------------------
    public static void saveCookie(String cookie, Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE).edit();
        editor.putString(cookieKey, cookie);
        editor.apply();
    }

    public static String getCookie(Context context){
        SharedPreferences preferences = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE);
        String cookie = preferences.getString(cookieKey, null);
        return cookie;
    }

    public static void deleteCookie(Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE).edit();
        editor.remove(cookieKey);
        editor.apply();
    }

    // --- EXP_DATE --------------------------------------------------------------------------------
    public static void saveExpDate(String expDate, Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE).edit();
        editor.putString(expDateKey, expDate);
        editor.apply();
    }

    public static String getExpDate(Context context){
        SharedPreferences preferences = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE);
        String expDate = preferences.getString(expDateKey, null);
        return expDate;
    }

    public static void deleteExpDate(Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE).edit();
        editor.remove(expDateKey);
        editor.apply();
    }

    // --- ALL -------------------------------------------------------------------------------------
    public static void deleteFullSession(Context context){
        deleteIdUser(context);
        deleteUsername(context);
        deleteCookie(context);
        deleteExpDate(context);
    }
}
