package com.x.projectmoon.roll;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.x.projectmoon.R;
import com.x.projectmoon.client.Client;
import com.x.projectmoon.dtos.RollCardDTO;
import com.x.projectmoon.utils.ImageDownloaderThread;
import com.x.projectmoon.utils.SharedPreferencesManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.nio.charset.StandardCharsets;

public class RollChooseActivity extends AppCompatActivity {

    public URL imageUrl;
    public int gems;
    public String name;
    public String description;
    public int idCard;
    private String username;
    private String cookie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        username = SharedPreferencesManager.getInstance().getUsername(getApplicationContext());
        cookie = SharedPreferencesManager.getInstance().getCookie(getApplicationContext());

        //Cogemos los datos de la otra actividad
        RollCardDTO cardObject = (RollCardDTO) getIntent().getSerializableExtra("RollCardDTO");

        imageUrl = cardObject.getUrlImage();
        gems = cardObject.getGems();
        name = cardObject.getName();
        description = cardObject.getDescription();
        idCard = cardObject.getIdCard();


        setContentView(R.layout.activity_roll_choose);

        Button chooseButton = findViewById(R.id.chooseButton);
        chooseButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                postRollChooseRest();

                //Métodos que cierran las ventanas
                finish();
                RollActivity.getInstance().finish();
            }
        });
        addDataPopup();
    }

    private void postRollChooseRest(){
        Client.getInstance(getApplicationContext()).postRollChooseRest(username, idCard, cookie,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Toast.makeText(RollChooseActivity.this, "Carta añadida a tu inventario", Toast.LENGTH_SHORT).show();
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    String responseBodyString = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                    try {
                        JSONObject errorResponseBodyJson = new JSONObject(responseBodyString);
                        Toast.makeText(RollChooseActivity.this, (String) errorResponseBodyJson.get("errorDescription"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException jsonE) {
                        jsonE.printStackTrace();
                    }
                    if (error.networkResponse.statusCode == 401){
                        Toast.makeText(RollChooseActivity.this, "Sesión inválida", Toast.LENGTH_SHORT).show();
                        SharedPreferencesManager.getInstance().deleteFullSession(getApplicationContext());
                    }
                }
            });

    }

    public void addDataPopup(){
        //Ponemos la imagen indicada en el popup
        ImageView card = findViewById(R.id.chooseImageview);
        ImageDownloaderThread iDT = new ImageDownloaderThread(imageUrl, card, this);
        iDT.start();

        TextView nameTV = findViewById(R.id.nameTextView);
        nameTV.setText(name);

        TextView descriptionTV = findViewById(R.id.descriptionTextView);
        descriptionTV.setText(description);

        TextView gemsTV = findViewById(R.id.gemsTextView);
        gemsTV.setText(String.valueOf(gems));
    }
}
