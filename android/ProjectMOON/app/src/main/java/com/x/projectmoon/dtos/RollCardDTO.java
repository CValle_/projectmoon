package com.x.projectmoon.dtos;

import java.io.Serializable;
import java.net.URL;

public class RollCardDTO implements Serializable {
    private int idCard;
    private String name;
    private String description;
    private URL urlImage;
    private int gems;

    public RollCardDTO(int idCard, String name, String description, URL urlImage, int gems) {
        this.idCard = idCard;
        this.name = name;
        this.description = description;
        this.urlImage = urlImage;
        this.gems = gems;
    }

    public int getIdCard() {
        return idCard;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }


    public URL getUrlImage() {
        return urlImage;
    }

    public int getGems() {
        return gems;
    }
}
