package com.x.projectmoon.client;

import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

//https://gitlab.afundacionfp.com/ruben.montero/league-skybreaker/-/blob/master/android/app/src/main/java/com/afundacionfp/leagueskybreaker/client/JsonObjectRequestSubclaseCustomHeader.java

public class JORSubclassCustomHeader extends JsonObjectRequest {
    private String cookie;

    public JORSubclassCustomHeader (int method, String url, String tokenSesion, @Nullable JSONObject jsonRequest, Response.Listener<JSONObject> listener, @Nullable Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
        this.cookie = tokenSesion;
    }
    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> overriddenHeaders = new HashMap<>();
        for (Map.Entry<String, String> entry : super.getHeaders().entrySet()) {
            overriddenHeaders.put(entry.getKey(), entry.getValue());
        }
        if (cookie != null) {
            overriddenHeaders.put("cookie", cookie);
        }
        return overriddenHeaders;
    }

}
