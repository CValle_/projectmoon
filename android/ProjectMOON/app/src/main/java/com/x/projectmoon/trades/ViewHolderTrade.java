package com.x.projectmoon.trades;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.x.projectmoon.R;

public class ViewHolderTrade extends RecyclerView.ViewHolder{

    private ImageView imageViewCardAsk;
    private ImageView imageViewCardOffer;
    private TextView textViewGemsAsk;
    private TextView textViewGemsOffer;
    private final Context context;


    public ViewHolderTrade(View itemView, Context context) {
        super(itemView);

        this.context = context;

        textViewGemsAsk = itemView.findViewById(R.id.gemsAsk);
        textViewGemsOffer = itemView.findViewById(R.id.gemsOffer);
        imageViewCardAsk = itemView.findViewById(R.id.cardAsk);
        imageViewCardOffer = itemView.findViewById(R.id.cardOffer);

    }
    public ImageView getImageViewCardAsk() {
        return imageViewCardAsk;
    }
    public ImageView getImageViewCardOffer() {
        return imageViewCardOffer;
    }
    public TextView getTextViewGemsAsk() {
        return textViewGemsAsk;
    }
    public TextView getTextViewGemsOffer() {
        return textViewGemsOffer;
    }

}
