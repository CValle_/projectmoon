package com.x.projectmoon;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.x.projectmoon.client.Client;
import com.x.projectmoon.inventory.InventoryActivity;
import com.x.projectmoon.roll.NotRollPopupActivity;
import com.x.projectmoon.roll.RollActivity;
import com.x.projectmoon.trades.TradesActivity;
import com.x.projectmoon.utils.SharedPreferencesManager;
import com.x.projectmoon.utils.TimerThread;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.Random;

public class MenuActivity extends AppCompatActivity {

	//private long count;
	private boolean firstRoll;

	private TextView timer;
	private TextView gems;
	private TextView usermameTV;
	private String username;
	private Context context;
	private TimerThread timerThreadT = null;
	private ImageView gachaB;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.context = this;

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_menu);

		firstRoll = getIntent().getBooleanExtra("firstRoll", true);

		refreshBackgroundImage();

		timer = findViewById(R.id.timer);
		gems = findViewById(R.id.gems);
		usermameTV = findViewById(R.id.username);

		username = SharedPreferencesManager.getInstance().getUsername(getApplicationContext());
		usermameTV.setText(username);

		//-- ON CLICK DEL GACHA BUTTON
		gachaB = findViewById(R.id.gachaButton);

		gachaButonLogic();

		//-- ON CLICK INVENTORY BUTTON
		ImageView intentoryB = findViewById(R.id.im_inventory);
		intentoryB.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(getApplicationContext(), InventoryActivity.class);
				startActivity(intent);
			}
		});

		//-- ON CLICK TRADE BUTTON
		ImageView tradeB = findViewById(R.id.im_trade);
		tradeB.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(getApplicationContext(), TradesActivity.class);
				startActivity(intent);
			}
		});

		//-- ON CLICK TRADE BUTTON
		ImageView shopB = findViewById(R.id.im_shop);
		shopB.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(getApplicationContext(), ShopActivity.class);
				startActivity(intent);
			}
		});

		//-- ON CLICK CERRAR SESIÓN
		ImageView closeB = findViewById(R.id.im_close);
		closeB.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				SharedPreferencesManager.getInstance().deleteFullSession(getApplicationContext());

				Intent intent = new Intent(getApplicationContext(), MainActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
			}
		});
	}

	@SuppressLint("ClickableViewAccessibility")
	public void gachaButonLogic(){
		if (firstRoll){
			gachaB.setImageDrawable(ContextCompat.getDrawable(MenuActivity.this, R.drawable.chestop));
			firstRoll = false;
		}

		gachaB.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent event) {
				switch(event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						gachaB.setImageDrawable(ContextCompat.getDrawable(MenuActivity.this, R.drawable.chestop));
						break;
					case MotionEvent.ACTION_UP:
						gachaB.setImageDrawable(ContextCompat.getDrawable(MenuActivity.this, R.drawable.chestclo));

						if (timerThreadT != null) {
							if (timerThreadT.isGachaTime()) {
								Intent intent = new Intent(getApplicationContext(), RollActivity.class);
								startActivity(intent);
							} else {
								Intent intent = new Intent(getApplicationContext(), NotRollPopupActivity.class);
								startActivity(intent);
							}
						} else {
							timerThreadT = new TimerThread(timer, 0L, context);
							timerThreadT.start();
							Intent intent = new Intent(getApplicationContext(), RollActivity.class);
							startActivity(intent);
						}

						break;
				}
				return true;
			}
		});
	}

	public void getUserLogic() {
		Client.getInstance(getApplicationContext()).getUserRest(username,
				new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						long count;

						try {
							gems.setText(String.valueOf(response.getInt("gems")));
							long lastRoll = response.getLong("last_roll");
							long timeNow = (long) System.currentTimeMillis();

							long timeSinceRoll = timeNow - lastRoll;
							count = 3600000 - timeSinceRoll;

							if (count <= 0)
								gachaB.setImageDrawable(ContextCompat.getDrawable(MenuActivity.this, R.drawable.chestop));

							timerThreadT = new TimerThread(timer, count, context);
							timerThreadT.start();

						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {

						if (timerThreadT != null)
							timerThreadT.setGachaTime(false);

						String responseBodyString = new String(error.networkResponse.data, StandardCharsets.UTF_8);
						try {
							//SE OBTIENE EL JSON DEL ERROR
							JSONObject errorResponseBodyJson = new JSONObject(responseBodyString);
							Toast.makeText(MenuActivity.this, (String) errorResponseBodyJson.get("errorDescription"), Toast.LENGTH_SHORT).show();
						} catch (JSONException jsonE) {
							jsonE.printStackTrace();
						}
					}
				});
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (timerThreadT != null)
			timerThreadT.interrupt();
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (timerThreadT != null)
			timerThreadT.interrupt();
	}

	@Override
	protected void onResume() {
		super.onResume();
		//Se para si hay otro para que no se dupliquen
		if (timerThreadT != null)
			timerThreadT.interrupt();

		//Se llama aquí al get para que se actualice cada vez que inicias esta pantalla
		getUserLogic();
		refreshBackgroundImage();
	}

	private void refreshBackgroundImage() {
		final ConstraintLayout backgroundLayout = findViewById(R.id.constraint_layout2);
		final ConstraintLayout pictureLayout = findViewById(R.id.background_layout);

		int[] images = {R.drawable.im_bckgrnd_1, R.drawable.im_bckgrnd_2, R.drawable.im_bckgrnd_3, R.drawable.im_bckgrnd_4, R.drawable.im_bckgrnd_5, R.drawable.im_bckgrnd_6};
		int[] bgColors = {R.color.im_bckgrnd_1, R.color.im_bckgrnd_2, R.color.im_bckgrnd_3, R.color.im_bckgrnd_4, R.color.im_bckgrnd_5, R.color.im_bckgrnd_6};
		Random random = new Random();
		int bgIndex = random.nextInt(images.length);

		pictureLayout.setBackgroundResource(images[bgIndex]);
		backgroundLayout.setBackgroundColor(getResources().getColor(bgColors[bgIndex]));
	}

}
