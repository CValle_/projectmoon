package com.x.projectmoon.inventory;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.x.projectmoon.R;

public class ViewHolderInventory extends RecyclerView.ViewHolder{
    private ImageView imageViewCard;
    private TextView textViewGems;
    private TextView textViewName;
    private TextView textViewDescription;
    private Button buttonRefund;


    public ViewHolderInventory(View itemView) {
        super(itemView);

        textViewGems = itemView.findViewById(R.id.gemasNumero);
        textViewName = itemView.findViewById(R.id.nombreCarta);
        textViewDescription = itemView.findViewById(R.id.descripcionCarta);
        imageViewCard = itemView.findViewById(R.id.carta);
        buttonRefund = itemView.findViewById(R.id.refundButton);

    }
    public ImageView getImageViewCard() {
        return imageViewCard;
    }
    public TextView getTextViewGems() {
        return textViewGems;
    }
    public TextView getTextViewName() {
        return textViewName;
    }
    public TextView getTextViewDescription() {
        return textViewDescription;
    }
    public Button getButtonRefund(){return buttonRefund;}

}
