package com.x.projectmoon.dtos;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CardListDTO {
    private JSONArray response;
    private List<CardDTO> inventoryDTOList;

    private int idCard;
    private int idFamily;
    private String name;
    private String description;
    private int gems;
    private URL urlImage;
    private URL urlShield;

    public CardListDTO(JSONArray response) {
        this.response = response;

        inventoryDTOList = new ArrayList<>();
        try {
            for (int i = 0; i < response.length(); i++) {
                idCard = response.getJSONObject(i).getInt("card_id");
                idFamily = response.getJSONObject(i).getInt("family_id");
                name = response.getJSONObject(i).getString("name");
                description = response.getJSONObject(i).getString("description");
                gems = response.getJSONObject(i).getInt("gems");
                urlImage = new URL (response.getJSONObject(i).getString("url_image"));
                urlShield = new URL (response.getJSONObject(i).getString("url_shield"));

                inventoryDTOList.add(new CardDTO(idCard, idFamily, name, description, gems, urlImage, urlShield));
            }
        } catch(JSONException | MalformedURLException e){
            e.printStackTrace();
        }
    }

    public List<CardDTO> getInventoryDTOList() {
        return inventoryDTOList;
    }
}
