package com.x.projectmoon.trades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.x.projectmoon.R;
import com.x.projectmoon.client.Client;
import com.x.projectmoon.dtos.TradeListDTO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

public class TradesActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private TradeListDTO tradeListDTO;
    private String myQuery = null;

    static TradesActivity tA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tA = this;
        setContentView(R.layout.activity_trades);

        getTradeListLogic();

        // -- BOTÓN DE CREAR TRADE
        Button chooseButton = findViewById(R.id.addTradeButton);
        chooseButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CreateTradeActivity.class);
                startActivity(intent);
            }
        });

        // -- BOTÓN DE REFRESH
        Button refreshButton = findViewById(R.id.refreshTradeButton);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                myQuery = null;
                getTradeListLogic();
            }
        });

        // -- SEARCH VIEW
        SearchView searchTrade = findViewById(R.id.tradesSearchView);
        searchTrade.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                myQuery = query;
                getTradeListLogic();

                return false;
            }
        });

        searchTrade.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                myQuery = null;
                getTradeListLogic();
                return false;
            }
        });
    }

    // -- LÓGICA DE LA PETICIÓN
    private void getTradeListLogic(){
        Client.getInstance(getApplicationContext()).getTradeListRest(myQuery,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    tradeListDTO = new TradeListDTO(response);

                    RecyclerAdapter adapter = new RecyclerAdapter(tradeListDTO.getTradeDTOList(), TradesActivity.this);

                    //RELACIONA EL ADAPTER CON EL RECYCLER VIEW
                    recyclerView = findViewById(R.id.tradesRecyclerView);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(TradesActivity.this));
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    String responseBodyString = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                    try {
                        JSONObject errorResponseBodyJson = new JSONObject(responseBodyString);
                        Toast.makeText(TradesActivity.this, (String) errorResponseBodyJson.get("errorDescription"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException jsonE) {
                        jsonE.printStackTrace();
                    }
                }
            });
    }

    public static TradesActivity getInstance(){
        return tA;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}