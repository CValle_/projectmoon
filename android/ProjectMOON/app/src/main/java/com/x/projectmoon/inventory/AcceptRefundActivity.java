package com.x.projectmoon.inventory;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.x.projectmoon.R;
import com.x.projectmoon.client.Client;
import com.x.projectmoon.utils.SharedPreferencesManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

public class AcceptRefundActivity extends AppCompatActivity {

    private String username;
    private String cookie;
    private int idCard;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        username = SharedPreferencesManager.getInstance().getUsername(getApplicationContext());
        cookie = SharedPreferencesManager.getInstance().getCookie(getApplicationContext());
        idCard = getIntent().getIntExtra("idCard", 0);

        setContentView(R.layout.activity_accept_refund);

        Button acceptRefundButton = findViewById(R.id.acceptRefund);

        acceptRefundButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                deleteCardLogic();
                finish();
            }
        });
    }

    public void deleteCardLogic(){
        Client.getInstance(getApplicationContext()).deleteCardRest(username, idCard, cookie,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(AcceptRefundActivity.this, "Carta vendida satisfactoriamente", Toast.LENGTH_SHORT).show();
                        InventoryActivity.getInstance().finish();
                    }
                },new Response.ErrorListener(){

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String responseBodyString = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                        try {
                            JSONObject errorResponseBodyJson = new JSONObject(responseBodyString);
                            Toast.makeText(AcceptRefundActivity.this, (String) errorResponseBodyJson.get("errorDescription"), Toast.LENGTH_SHORT).show();

                            if (error.networkResponse.statusCode == 401){
                                //Toast.makeText(AcceptTradeActivity.this, "Sesión inválida", Toast.LENGTH_SHORT).show();
                                SharedPreferencesManager.getInstance().deleteFullSession(getApplicationContext());
                            }
                        } catch (JSONException jsonE) {
                            jsonE.printStackTrace();
                        }
                    }
                });
    }

}