# #7: Discutir sobre tecnología para las apps desktop/mobile

Tras los cambios efectuados, el equipo projectMOON ha decido centrarse en el desarrollo de las siguientes apps de forma conjunta:

* Android: Se continuará desarrollando la aplicación con normalidad, con los mismos mockups y con el IDE AndroidStudio. La diferencia es que ahora nos repartiremos las tareas entre todas las personas del equipo y desarollaremos la aplicación de forma conjunta.

* Django: Con este desarollaremos el API rest y la web. Como en el anterior punto, ahora pasará de ser trabajo de una persona, a que todos los integrantes del equipo nos encarguemos de aprender las diferentes funcionalidades de este lenguaje, así como el desarrollo del API rest y de aprender alguna implementación con CSS y HTML.

* Desktop: Por otra parte, la aplicación de Desktop (con flutter) quedará relegada a un segundo plano y se intentarán implementar cosas en la medida de lo posible, siempre que el tiempo lo permita.
