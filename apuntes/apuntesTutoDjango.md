# TUTORIAL DJANGO

### PART 1 - 1.1 Creating a project
* En la carpeta en la que desea crear el proyecto (cd ruta/)

```
$ django-admin startproject mysite
```
* Cambiamos al directorio que acabamos de crear (cd mysite/) y ejecutamos el servidor

```
$ python manage.py runserver
```
Si se especifica un puerto (ej. 8080) al final de este comando, se iniciará directamente en el


### 1.2 Creating the Polls app

> APPS VS PROJECTS: A project is a collection of configuration and apps for a particular website. A project can contain multiple apps. An app can be in multiple projects.

* Ejecutaremos el siguiente comando en el mismo directorio que el anterior. Estamos creando la carpeta de la app Polls

```
$ python manage.py startapp polls
```

### 1.3 Write your first view
* Abrimos el archivo polls/views.py y añadimos el siguiente código python
```
from django.http import HttpResponse

def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")
```
Esta es la vista más simple posible en Django. Para llamar la vista, tenemos que asignarla a una URL

* Cree un archivo llamado urls.py en el directorio polls/ e incluya el siguiente código python
```
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
]
```
* En mysite/urls.py (ya existente) incluya el siguiente código python
```
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('polls/', include('polls.urls')),
    path('admin/', admin.site.urls),
]
```

La función include() permite hacer referencia a otros URLconfs. Cada vez que Django encuentra include() corta cualquier parte de la URL que coincide hasta ese punto y envía la cadena restante a la URLconf incluida para seguir el proceso.

* Ahora ha conectado una vista de índice en URLconf. Vuelva a iniciar el servidor para comprobar que funciona:
```
$ python manage.py runserver
```
Y vaya a http://localhost:8000/polls/ en su ordenador

### PART 2 - 2.1 Database setup
* Abrimos el archivo settings.py y le cambiamos la zona horaria a la nuestra (Apartado # Internationalization)
```
TIME_ZONE = 'Europe/Madrid'
```
* Desde el directorio raíz ejecutamos el siguiente comando para crear la BBDD necesaria para las apps
```
$ python manage.py migrate
```

### 2.2 Creating models

* Vamos a crear los modelos Question y Choice, que son representados como clases de python. En polls/models.py añadimos el siguiente código python
```
from django.db import models

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
```

### 2.3 Activating models

* Para incluir la aplicación en nuestro proyecto necesitamos agregar la app a INSTALLED_APPS en el archivo mysite/settings.py
```
INSTALLED_APPS = [
    'polls.apps.PollsConfig',
    ...,
]
```
* Guía de tres pasos para hacer cambios de modelo:
```
    #Cambie sus modelos (en models.py).
    #Ejecute el comando siguienre para crear migraciones para esos cambios
    $ python manage.py makemigrations [name]
    #Ejecute el comando siguiente para aplicar esos cambios a la base de datos.
    $ python manage.py migrate
```

### 2.4 Introducing the Django Admin
* Creando un usuario del admin. Ejecute el siguiente comando
```
$ python manage.py createsuperuser
#Tendrá que introuducir su usuario, correo y contraseña
```
* El sitio administrativo de Django se activa de forma predeterminada. Vamos a iniciar el servidor de desarrollo.
```
$ python manage.py runserver
```
Y entraremos con la siguiente url http://127.0.0.1:8000/admin/ y acceda con el usuario y contraseña que acaba de crear

* Ahora hagamos que la app se pueda ejecutar desde el sitio administrativo. Agrega el siguiente código en polls/admin.py
```
from django.contrib import admin
from .models import Question, Choice

admin.site.register(Question)
admin.site.register(Choice)
#La opción choice no está incluída en el tutorial, pero es necesaria para añadir opciones desde la pantalla de admin

```
* Al agregar una pregunta es posible que te la nombre como 'Question object (1)'. Podemos arreglar esto añadiendo el siguiente código python a polls/models.py
```
class Question(models.Model):
    # ...
    def __str__(self):
        return self.question_text

class Choice(models.Model):
    # ...
    def __str__(self):
        return self.choice_text
```

* También vamos a añadir la siguiente clase custom
```
import datetime
...
from django.utils import timezone

class Question(models.Model):
    # ...
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
```
### PART 3 - 3.1 Writing more views
* Vamos a agregar más vistas a polls/views.py. Estas vistas son un poco diferentes porque toman un argumento:
```
def detail(request, question_id):
    return HttpResponse("You're looking at question %s." % question_id)

def results(request, question_id):
    response = "You're looking at the results of question %s."
    return HttpResponse(response % question_id)

def vote(request, question_id):
    return HttpResponse("You're voting on question %s." % question_id)
```
* Vamos a añadir estas nuevas vistas a polls/urls.py con las siguientes llamadas path():
```
urlpatterns = [
    # ex: /polls/
    #path('', views.index, name='index'),
    # ex: /polls/5/
    path('<int:question_id>/', views.detail, name='detail'),
    # ex: /polls/5/results/
    path('<int:question_id>/results/', views.results, name='results'),
    # ex: /polls/5/vote/
    path('<int:question_id>/vote/', views.vote, name='vote'),
]
```

### 3.2 Write views that actually do something¶
* Añada a polls/views.py el siguiente código para crar una nueva vista index() que muestra en el sistema las 5 últimas preguntas de la encuesta
```
from django.http import HttpResponse
from .models import Question

def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    output = ', '.join([q.question_text for q in latest_question_list])
    return HttpResponse(output)

#Puede dar un error ya que el atributo objects no existe, se crea mientras se ejecuta. El reso se dejan sin cambiar.
```
* Ahora añadiremos el diseño creando las carpetas templates y polls y en esta última un archivo index.html de forma que quedasen así:
```
polls/templates/polls/index.html
```
* En este archivo que acabamos de cerear añadimos el siguiente código python:
```
{% if latest_question_list %}
    <ul>
    {% for question in latest_question_list %}
        <li><a href="/polls/{{ question.id }}/">{{ question.question_text }}</a></li>
    {% endfor %}
    </ul>
{% else %}
    <p>No polls are available.</p>
{% endif %}

#EL HTML DEL TUTORIAL PUEDE ESTAR INCOMPLETO
```

* Ahora vamos a actualizar nuestra vista index en polls/views.py para usar la plantilla (USA RENDER, NO ESTO):
```
from django.http import HttpResponse
from django.template import loader

from .models import Question

def index(request):
    #El atributo objects no existe, se crea mientras se ejecuta
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    template = loader.get_template('polls/index.html')
    context = {
        'latest_question_list': latest_question_list,
    }
    return HttpResponse(template.render(context, request))
```

* Es una práctica muy común cargar una plantilla, llenar un contexto y retornar un objeto HttpResponse con el resultado de la plantilla creada. Django proporciona un atajo. RENDER.
```
from django.shortcuts import render
#from django.http import HttpResponse
from .models import Question

def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {'latest_question_list': latest_question_list}
    return render(request, 'polls/index.html', context)

    #El resto de las funciones deberían estar como esta, pero se cambiarán más adelante
```
### 3.3 Raising a 404 error
* Actualizamos la vista de detalle de la pregunta para una encuesta determinada. Modificamos la función Detail en polls/views.py (ESTO NO, USAR EL ATAJO DEL SIGUIENTE PUNTO):
```
from django.http import Http404
from django.shortcuts import render

from .models import Question
# ...
def detail(request, question_id):
    try:
        question = Question.objects.get(pk=question_id)
    except Question.DoesNotExist:
        raise Http404("Question does not exist")
    return render(request, 'polls/detail.html', {'question': question})
```

Esto nos presenta un nuevo concepro: La vista levanta la excepción Http404 si no existe una pregunta con la ID solicitada. 
> Podemos empezar creando el archivo polls/templates/polls/detail.html

* UN ATAJO get_object_or_404(). Es una práctica muy común utilizar get() y levantar la excepción Http404 si no existe el objeto. Django proporciona un atajo:
```

polls/views.py¶

from django.shortcuts import get_object_or_404, render

from .models import Question
# ...
def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/detail.html', {'question': question})
```

### 3.4 Use the template system
* Añadimos el siguiente código al archivo detail.html que recomendamos crear anteriormente
```
<h1>{{ question.question_text }}</h1>
<ul>
{% for choice in question.choice_set.all %}
    <li>{{ choice.choice_text }}</li>
{% endfor %}
</ul>
```
Para más información sobre las plantillas https://docs.djangoproject.com/en/3.1/topics/templates/

### 3.5 Removing hardcoded URLs in templates
Cuando escribimos el enlace para una pregunta en la plantilla polls/index.html, el enlace estaba codificado de forma predeterminada. El problema con este método es que se hace difícil modificar las URLs en proyectos que tengan muchas plantillas, pero como hemos definido las funciones path() en el polls.urls, podemos eliminar esa dependencia de la siguiente forma
* Modificamos el código de intex.html al siguiente código
```
<li><a href="{% url 'detail' question.id %}">{{ question.question_text }}</a></li>
```
Así si se quiere modificar la URL de la vista de detalle de las encuestas a algo diferentese podría hacer directamente modigicando la en polls/urls.py:

### 3.6 Namespacing URL names
Cuando tenemos varias apps en nuestro proyecto, para diferenciarlas, añadimos espacios de nombres a su URLconf. 
* Añade en polls/urls.py un app_name para configurar el espacio de nombres de la aplicación con el siguiente código
```
app_name = 'polls'

urlpatterns = [ ...
```

* Ahora modifique su plantilla polls/index.html con el siguiente código
```
<li><a href="{% url 'polls:detail' question.id %}">{{ question.question_text }}</a></li>
```

### PART 4 - 4.1 Write a minimal form
* Vamos a actualizar nuestra plantilla de detalles de encuestas («polls/detail.html») para que contenga un elemento HTML <form>
```
<h1>{{ question.question_text }}</h1>

{% if error_message %}<p><strong>{{ error_message }}</strong></p>{% endif %}

<form action="{% url 'polls:vote' question.id %}" method="post">
{% csrf_token %}
{% for choice in question.choice_set.all %}
    <input type="radio" name="choice" id="choice{{ forloop.counter }}" value="{{ choice.id }}">
    <label for="choice{{ forloop.counter }}">{{ choice.choice_text }}</label><br>
{% endfor %}
<input type="submit" value="Vote">
</form>
```

A continuación vamos a crear una vista Django que maneje los datos enviados y haga algo con ellos (en la parte anterior ya creamos un URLConf con path() y una view con la función vote())

* Vamos a modificar lo siguietne en polls/views.py
```
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse

from .models import Choice, Question

# ...
def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))
        # Esta URL redirigida entonces llamará a la vista 'results' para mostrar la página final
```

* Después de que alguien vota en una pregunta, la vista vote() remite a la página de resultados de la pregunta. Modificamos polls/views.py para ello:
```
def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/results.html', {'question': question})
```

Esto es casi exactamente lo mismo que la vista detail() de la parte 3. La única diferencia es el nombre de la plantilla. Solucionaremos esta redundancia más tarde.

* Ahora, cree una plantilla polls/results.html e inserte el siguiente código:
```
<h1>{{ question.question_text }}</h1>

<ul>
{% for choice in question.choice_set.all %}
    <li>{{ choice.choice_text }} -- {{ choice.votes }} vote{{ choice.votes|pluralize }}</li>
{% endfor %}
</ul>

<a href="{% url 'polls:detail' question.id %}">Vote again?</a>
```

Ahora vaya a /polls/1/ en su navegador y vote en la pregunta. Debería ver una página de resultados que se actualiza cada vez que usted vota. Si usted envía el formulario sin haber seleccionado una opción, usted debería ver un mensaje de error.

### 4.2 Use generic views: Less code is better

La función detail() y la vista results() son muy cortas, y como dijimos antes, redundantes. La vista index() que muestra una lista de polls, es similar.

> Generalmente, cuando se escribe una aplicación de Django, usted podrá utilizar las vistas genéricas desde el principio en lugar de reestructurar su código a medio camino. Sin embargo, este tutorial intencionadamente se ha centrado hasta ahora en escribir las vistas «de la manera difícil» para centrarse en los conceptos esenciales

Vamos a convertir nuestra poll app para usar generic views, lo que nos ahorrará mucho código. Seguiremos los siguientes pasos para cambiarlo:

* Convertir el URLconf.
* Eliminar algunas de las vistas viejas e innecesarias.
* Introducir nuevas vistas basadas en las vistas genéricas de Django.

### 4.2 Amend URLconf

* Primero, abra el URLconf polls/urls.py y modifíquelo de la siguiente manera:
```
from django.urls import path

from . import views

app_name = 'polls'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
]

#Los path strings de los patrones segundo y tercero ha cambiado de <question_id> a <pk>.
```
### 4.3 Amend views

* A continuación, vamos a eliminar nuestras viejas vistas index, detail y results y en su lugar vamos a usar las vistas genéricas de Django. Para ello, abra el archivo polls/views.py y modifíquelo de la siguiente manera
```
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic

from .models import Choice, Question


class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.order_by('-pub_date')[:5]


class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'


def vote(request, question_id):
    ... # same as above, no changes needed.
```

Cada vista genérica tiene que saber cuál es el modelo sobre el que estará actuando. Esto se proporciona utilizando el atributo model.

La vista genérica DetailView espera que el valor de la clave primaria capturado desde la URL sea denominado "pk", por lo que hemos cambiado ``question_id`` a ``pk`` para las vistas genéricas.

