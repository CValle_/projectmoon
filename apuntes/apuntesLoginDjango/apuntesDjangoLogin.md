# APUNTES DJANGO LOGIN

### Iniciar proyecto y crear app
Ejecutaremos los siguientes comandos:

```
$ django-admin startproject mi_login
$ cd mi_login
$ python manage.py startapp mi_login0118
```

### Vincular la aplicación con el proyecto
Modificamos el archivo urls.py para que el código quede de esta forma:

> mi_login/mi_login/urls.py
```
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('mi_login0118.urls')),
    path('admin/', admin.site.urls),
]
```

### Crar urls.py en la app
Crear el archivo e introducir la ruta para vincularlo con el API Rest

> mi_login/mi_login0118/urls.py
```
from django.urls import path

from . import rest_facade

urlpatterns = [
    path('user/<username>/login', rest_facade.login, name='login'),
]
```
### Crear rest_facade.py a la app y añadir un pequeño test
Comenzamos a crear la fachada rest 

> mi_login/mi_login0118/rest_facade.py
```
from django.shortcuts import render

def login(request, username):
    print('Funciona')
```
y comprobar si funciona: ` $ python manage.py runserver`
![](pruebaPath.png)

### Creamos el modelo para la BBDD (parte principal del API Rest)
Modificamos models.py para que Django cree por nosotros la base de datos.

> mi_login/mi_login0118/models.py
```
from django.db import models

class Usuario(models.Model):
    #max_length required
    nombre = models.CharField(max_length=200, unique=True)
    contrasena = models.CharField(max_length=100)
    token_sesion= models.CharField(max_length=100, )
    creacion_date = models.DateTimeField(auto_now_add=True) #First time
    fecha_modificacion = models.DateTimeField(auto_now=rue) #Modify
    #id se genera automáticamente
```
podemos añadir el método to string de python (\_\_str\_\_) para que en vez de mostrarte el objeto '“Usuario object (1)”' se muestre el nombre que le indicamos al modelo.

```
class Usuario(models.Model):
    #...
    def __str__(self):
        return self.nombre
```

### Conectamos el modelo en settings.py

> mi_login/mi_login/settings.py
```
INSTALLED_APPS = [
    'mi_login0118.apps.MiLogin0118Config',
    #...
]
```

### Y creamos la base de datos con makemigrations y migrate
Estos comandos se ejecutan siempre que modifiquemos un modelo

```
$ python manage.py makemigrations mi_login0118
$ python manage.py migrate
```

### Añadimos el sitio de Admin
Creamos el usuario del admin añadiendo el siguiente comando

```
$ python manage.py createsuperuser
#Tendrá que introuducir su usuario, correo y contraseña
```

También modificamos el siguiente archivo

> mi_login/mi_login0118/admin.py
```
from django.contrib import admin

from .models import Usuario

admin.site.register(Usuario)

```

y comprobar si funciona: ` $ python manage.py runserver`

### Comenzamos con el endpoint rest para el login del usuario
Escribiremos el siguiente código para obtener la contraseña del cuerpo de la petición

> mi_login/mi_login0118/rest_facade.py
```
from django.shortcuts import render
from django.http import HttpResponse

from .models import Usuario

def login(request, username):
    usuario = Usuario.objects.get(nombre__exact=username) #__exact matchea exactamente con la bbdd

    return HttpResponse (usuario.contrasena, status=200)
```

### Procedemos a la interpretación del (JSON) request body
Añadimos el siguiente código al archivo anterior

```
import json
#...

def login(request, username):
    #usuario = ...

    #traduce stream a diccionario
    request_body = json.loads(request.body)

    #COMPROBACIONES
    print ('--- CUERPO --------------')
    print (request_body)

    #return HttpResponse ...

```
Deberemos añadir también el decorador csrf (Cross-site request forgery), ya que al hacer las cosas más manuales, lo que genera automáticamente Django causa conflicto

```
#...
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def login(request, username):
#...
```

Comprobamos que el cliente funciona con `curl -v -X PUT "127.0.0.1:8000/admin/mi_login0118/usuario/" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"contrasena\":\"abc123..\"}"`

![](pruebaBody.png)

### Añadimos el if que comprueba las contraseñas y creamos el token con secrets
Obtenemos las contraseñas de la request y de la base de datos, las comparamos y, en caso de ser correctas, creamos (con secrets) y devolvemos un token aleatorio, en caso contrario devolvemos un 401. El código quedaría de la siguiente forma:

> mi_login/mi_login0118/rest_facade.py
```
import secrets
#...

@csrf_exempt
def login(request, username):

    usuario = Usuario.objects.get(nombre__exact=username)

    #traduce stream a diccionario, obtenemos las pass
    request_body = json.loads(request.body)
    password_in_request = request_body.get('contrasena')
    password_in_database = usuario.contrasena

    if password_in_request == password_in_database:
        #Si coinciden devolvemos token
        aleatorio = secrets.token_urlsafe(64)
        nuevo_token = str(usuario.id) + '-' + aleatorio

        return HttpResponse ('LAS CONTRASEÑAS COINCIDEN', status=200)
    
    return HttpResponse ('CONTASEÑA INCORRECTA', status=401)
```

### Cambiamos a devolverlo todo con JSON

```
from django.http import HttpResponse, JsonResponse
#...

def login(request, username):
    #...

    if password_in_request == password_in_database:
        #Si coinciden devolvemos token
        aleatorio = secrets.token_urlsafe(64)
        nuevo_token = str(usuario.id) + '-' + aleatorio
        mi_respuesta = {"session_cookie" : nuevo_token}

        return JsonResponse(mi_respuesta, status=200)
    
    return JsonResponse({"errorDescription": "Las contraseñas no coinciden, acceso denegado"}, status=401)
```

Comprobamos que funciona con la contraseña incorrecta `curl -v -X PUT "127.0.0.1:8000/admin/mi_login0118/usuario/" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"contrasena\":\"passmal\"}"`

* respuesta cliente ---------------------
![](JSONCliente.png)
* respuesta servidor --------------------
![](JSONServidor.png)

### Comprobamos que el método sea soportado por el servidor (405)

```
from django.http import HttpResponse, HttpResponseNotAllowed, JsonResponse
#...

@csrf_exempt
def login(request, username):
    #si recibimos una petición que no sea put, devolvemos 405
    if request.method != 'PUT':
        return HttpResponseNotAllowed(['PUT']) #lista de las soportadas

    #...
```

### Comprobamos que el username exista (404)
Lo haremos rodeando el get del usuario con un try catch
```
@csrf_exempt
def login(request, username):
    #if...
    
    try:
        usuario = Usuario.objects.get(nombre__exact=username)
    except Usuario.DoesNotExist:
        return JsonResponse({"errorDescription": "Usuario no encontrado, pruebe a registrarse"}, status=404)
```

### Arreglar server response
Para ello vamos a usar CORS headers. Vamos a instalarlo en el proyecto:
```
$ python -m pip install django-cors-headers
```

A continuación añadiremos CORS a INSTALLED.APPS y al MIDDLEWARE:

> mi_login/mi_login/settings.py
```
INSTALLED_APPS = [
    'corsheaders',
    #...
]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    #...
]
```

Por último lo activaremos para cuando el modo debug esté en true:

> mi_login/mi_login/settings.py
```
DEBUG = True

ALLOWED_HOSTS = []

CORS_ALLOW_ALL_ORIGINS = DEBUG
```

