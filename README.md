# projectMOON

### ACTUALIZAR LAS RAMAS DESPUÉS DE UN MERGE
```
$ git checkout master
$ git fetch
$ git pull
```
### EN CASO DE CONFLICTO AL HACER MERGE
```
$ git checkout rama-conflictiva
$ git merge master 

# (traigo master y resuelvo los conflictos a mano, hacer push e intentar mergear de nuevo)
```

### CREAR RAMA
* $ git checkout -b nombre-de-la-rama
### CAMBIAR A LA RAMA INDICADA
* $ git checkout nombre-de-la-rama
### SABER EN QUE RAMA ESTOY (Y que ramas hay)
* $ git branch
### BORRAR RAMA INDICADA
* $ git branch -d nombre-de-la-rama
### DESHACER UN COMMIT
Donde el número es el commit al que quieres regresar
* $ git reset --hard 5790148b
